{
    "id": "3cb7644d-4206-4ba4-828e-6d4dde20e71b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "blackcatyneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 5,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b3007fb-897c-4707-b5bf-dd8d3e1822e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3cb7644d-4206-4ba4-828e-6d4dde20e71b",
            "compositeImage": {
                "id": "4a80c7ce-b92b-4ee5-986d-1b26c64a1731",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b3007fb-897c-4707-b5bf-dd8d3e1822e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d2afc14-aff5-4558-89d5-c0f5f891dbc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b3007fb-897c-4707-b5bf-dd8d3e1822e3",
                    "LayerId": "340fba63-c924-4677-abd9-7df71e7ff06b"
                }
            ]
        },
        {
            "id": "e9a5a2ec-e2fd-42a3-8003-65b818077293",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3cb7644d-4206-4ba4-828e-6d4dde20e71b",
            "compositeImage": {
                "id": "1c99b19e-c5b1-48cd-8c31-2ec634468cda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9a5a2ec-e2fd-42a3-8003-65b818077293",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "512ad629-a5a6-4e86-ad32-f41ad93e5aff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9a5a2ec-e2fd-42a3-8003-65b818077293",
                    "LayerId": "340fba63-c924-4677-abd9-7df71e7ff06b"
                }
            ]
        },
        {
            "id": "b4879c14-d4c9-49c6-a4a7-743a660fab4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3cb7644d-4206-4ba4-828e-6d4dde20e71b",
            "compositeImage": {
                "id": "361f23da-355b-4959-a444-5cc321bd17fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4879c14-d4c9-49c6-a4a7-743a660fab4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6615762b-c28b-474b-9803-e1d7f5cc3d44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4879c14-d4c9-49c6-a4a7-743a660fab4d",
                    "LayerId": "340fba63-c924-4677-abd9-7df71e7ff06b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "340fba63-c924-4677-abd9-7df71e7ff06b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3cb7644d-4206-4ba4-828e-6d4dde20e71b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 0,
    "yorig": 0
}