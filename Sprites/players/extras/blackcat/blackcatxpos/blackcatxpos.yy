{
    "id": "9c15cab6-cabe-4dc2-bbae-a7359473fae2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "blackcatxpos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 1,
    "bbox_right": 28,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec050b36-da4b-404f-a63d-27e133ea58b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c15cab6-cabe-4dc2-bbae-a7359473fae2",
            "compositeImage": {
                "id": "29c116aa-8c5f-49fd-a2b3-2fc9ca78c1e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec050b36-da4b-404f-a63d-27e133ea58b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b71b5d28-95fd-485b-a1e8-40258917ba16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec050b36-da4b-404f-a63d-27e133ea58b3",
                    "LayerId": "4ea2b221-227a-45ce-8579-d5b630e6dc1f"
                }
            ]
        },
        {
            "id": "ebdc1138-c2ee-4c0d-a10b-9641d160e5c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c15cab6-cabe-4dc2-bbae-a7359473fae2",
            "compositeImage": {
                "id": "686f1a9b-46cf-4548-b10c-ba705f707b32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebdc1138-c2ee-4c0d-a10b-9641d160e5c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12f1db7a-c106-429c-9603-e07d41597988",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebdc1138-c2ee-4c0d-a10b-9641d160e5c5",
                    "LayerId": "4ea2b221-227a-45ce-8579-d5b630e6dc1f"
                }
            ]
        },
        {
            "id": "0bfc1224-5b57-4a07-af18-4308f96d7507",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c15cab6-cabe-4dc2-bbae-a7359473fae2",
            "compositeImage": {
                "id": "3e52d021-4da4-4df4-80f9-53ef59667b67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bfc1224-5b57-4a07-af18-4308f96d7507",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41b4d328-45b8-4b31-bc2c-1aa874dbffc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bfc1224-5b57-4a07-af18-4308f96d7507",
                    "LayerId": "4ea2b221-227a-45ce-8579-d5b630e6dc1f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "4ea2b221-227a-45ce-8579-d5b630e6dc1f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c15cab6-cabe-4dc2-bbae-a7359473fae2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 0,
    "yorig": 0
}