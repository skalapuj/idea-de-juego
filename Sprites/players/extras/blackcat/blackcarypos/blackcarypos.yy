{
    "id": "68917642-e195-49d2-9797-467093933ffe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "blackcarypos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 10,
    "bbox_right": 25,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e8f570b0-e084-4b6a-b4a2-e5a1401e6338",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68917642-e195-49d2-9797-467093933ffe",
            "compositeImage": {
                "id": "1b8d9da6-0496-4c08-86c5-680822d6d6b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8f570b0-e084-4b6a-b4a2-e5a1401e6338",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fef890f0-355b-4e11-abe2-ee86ccc86995",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8f570b0-e084-4b6a-b4a2-e5a1401e6338",
                    "LayerId": "986c00c2-a0f5-4feb-ad29-fe9d3b5c24e5"
                }
            ]
        },
        {
            "id": "ca38356a-7523-48a4-b8e1-6093d4cd9adb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68917642-e195-49d2-9797-467093933ffe",
            "compositeImage": {
                "id": "164ee297-fcb0-4987-83e5-d1435957f678",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca38356a-7523-48a4-b8e1-6093d4cd9adb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac3c6311-d3b9-4746-a3ec-ab598a7e9c88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca38356a-7523-48a4-b8e1-6093d4cd9adb",
                    "LayerId": "986c00c2-a0f5-4feb-ad29-fe9d3b5c24e5"
                }
            ]
        },
        {
            "id": "316fd222-0c7e-4172-b8ad-f36cab7f4b6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68917642-e195-49d2-9797-467093933ffe",
            "compositeImage": {
                "id": "3cb24762-179d-4f69-ba8b-824b81685c17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "316fd222-0c7e-4172-b8ad-f36cab7f4b6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6c18bcc-ad31-46ae-927d-4211821d9737",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "316fd222-0c7e-4172-b8ad-f36cab7f4b6b",
                    "LayerId": "986c00c2-a0f5-4feb-ad29-fe9d3b5c24e5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "986c00c2-a0f5-4feb-ad29-fe9d3b5c24e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68917642-e195-49d2-9797-467093933ffe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 0,
    "yorig": 0
}