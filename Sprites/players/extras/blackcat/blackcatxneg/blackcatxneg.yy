{
    "id": "48ed5ca5-80a9-4a00-835f-1dc828e3f9df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "blackcatxneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "52c731c3-5e8a-4b9d-afef-f06e4f9ea0dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48ed5ca5-80a9-4a00-835f-1dc828e3f9df",
            "compositeImage": {
                "id": "ae3b776d-1998-433d-83a4-713c715e306a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52c731c3-5e8a-4b9d-afef-f06e4f9ea0dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21aa0df4-0654-44ba-a77f-551fbaa7f918",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52c731c3-5e8a-4b9d-afef-f06e4f9ea0dc",
                    "LayerId": "47446688-a933-4a45-9526-f4710f9f8f92"
                }
            ]
        },
        {
            "id": "32afc217-3327-444a-82dc-5b531f10fe0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48ed5ca5-80a9-4a00-835f-1dc828e3f9df",
            "compositeImage": {
                "id": "2eddb8d4-3d6e-43e0-bb6c-b132b962e900",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32afc217-3327-444a-82dc-5b531f10fe0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "872e6912-3c86-4f86-8fe8-e4670b452fb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32afc217-3327-444a-82dc-5b531f10fe0b",
                    "LayerId": "47446688-a933-4a45-9526-f4710f9f8f92"
                }
            ]
        },
        {
            "id": "b7d3f58f-0785-4d51-8b47-785517e59651",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48ed5ca5-80a9-4a00-835f-1dc828e3f9df",
            "compositeImage": {
                "id": "7d824299-37b5-46c0-89be-f6cbfaa94ffc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7d3f58f-0785-4d51-8b47-785517e59651",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91cefe1f-8f68-46fc-9fc6-3fcee46472e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7d3f58f-0785-4d51-8b47-785517e59651",
                    "LayerId": "47446688-a933-4a45-9526-f4710f9f8f92"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "47446688-a933-4a45-9526-f4710f9f8f92",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48ed5ca5-80a9-4a00-835f-1dc828e3f9df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 0,
    "yorig": 0
}