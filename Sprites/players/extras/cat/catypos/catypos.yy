{
    "id": "76749a27-e3bb-4084-a9a1-de1d3a91bf01",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "catypos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 9,
    "bbox_right": 22,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c59e65bc-327d-435c-80aa-420fc74854a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76749a27-e3bb-4084-a9a1-de1d3a91bf01",
            "compositeImage": {
                "id": "38f745d0-9db4-4fe1-b81f-ee0e254aea84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c59e65bc-327d-435c-80aa-420fc74854a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "829d4037-952e-415d-a72e-c275dfea2c87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c59e65bc-327d-435c-80aa-420fc74854a0",
                    "LayerId": "3237f17e-2d56-4eec-9be5-e07ae2df18a9"
                }
            ]
        },
        {
            "id": "306034c8-122a-4b66-9b3a-c2074ad6dc5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76749a27-e3bb-4084-a9a1-de1d3a91bf01",
            "compositeImage": {
                "id": "26699b9e-ffe8-4ec1-99c1-270cc8cf7734",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "306034c8-122a-4b66-9b3a-c2074ad6dc5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cf56897-67f7-4935-b88c-47a36e159bdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "306034c8-122a-4b66-9b3a-c2074ad6dc5f",
                    "LayerId": "3237f17e-2d56-4eec-9be5-e07ae2df18a9"
                }
            ]
        },
        {
            "id": "a12a5405-13fb-4230-8a54-35d96edb9ca2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76749a27-e3bb-4084-a9a1-de1d3a91bf01",
            "compositeImage": {
                "id": "1bd715a5-f4ff-40dc-808c-29d4b096da75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a12a5405-13fb-4230-8a54-35d96edb9ca2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8e7e1aa-87a5-40a2-a2da-e06732e44e38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a12a5405-13fb-4230-8a54-35d96edb9ca2",
                    "LayerId": "3237f17e-2d56-4eec-9be5-e07ae2df18a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3237f17e-2d56-4eec-9be5-e07ae2df18a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76749a27-e3bb-4084-a9a1-de1d3a91bf01",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}