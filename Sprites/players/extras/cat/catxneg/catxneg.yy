{
    "id": "b65e0517-e34e-4e4b-8565-ca5017af36d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "catxneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87d25183-1945-4614-82df-1e1ee23008c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b65e0517-e34e-4e4b-8565-ca5017af36d4",
            "compositeImage": {
                "id": "11048c64-0909-4661-8264-5f55287fa078",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87d25183-1945-4614-82df-1e1ee23008c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5249e0d7-f995-4c2b-8907-219e02fbcdb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87d25183-1945-4614-82df-1e1ee23008c9",
                    "LayerId": "077b1d0a-bcd4-4f71-ab13-88ce946b2ccd"
                }
            ]
        },
        {
            "id": "ec2a660f-85ec-41b0-9458-9a87903a02e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b65e0517-e34e-4e4b-8565-ca5017af36d4",
            "compositeImage": {
                "id": "2a4c8d41-c87e-4fad-9c0b-fab1bb186cd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec2a660f-85ec-41b0-9458-9a87903a02e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28e4ede5-0bbe-475f-9454-e81285683538",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec2a660f-85ec-41b0-9458-9a87903a02e7",
                    "LayerId": "077b1d0a-bcd4-4f71-ab13-88ce946b2ccd"
                }
            ]
        },
        {
            "id": "00f7981b-8a97-46c6-960f-447427408862",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b65e0517-e34e-4e4b-8565-ca5017af36d4",
            "compositeImage": {
                "id": "f0eda7c1-318d-4e92-bbf8-f992a1ce170f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00f7981b-8a97-46c6-960f-447427408862",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cd300b0-469f-4945-a006-0b39617c42d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00f7981b-8a97-46c6-960f-447427408862",
                    "LayerId": "077b1d0a-bcd4-4f71-ab13-88ce946b2ccd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "077b1d0a-bcd4-4f71-ab13-88ce946b2ccd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b65e0517-e34e-4e4b-8565-ca5017af36d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}