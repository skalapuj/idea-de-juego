{
    "id": "3dde975c-329b-4440-b3c0-592cbb019daa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "catyneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 9,
    "bbox_right": 22,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "23995819-7143-49d7-8e27-bbe43e35a2bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dde975c-329b-4440-b3c0-592cbb019daa",
            "compositeImage": {
                "id": "1b2adafc-272a-4e01-bb71-eca0892182b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23995819-7143-49d7-8e27-bbe43e35a2bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cc82be6-1ee9-485b-9132-98210384644d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23995819-7143-49d7-8e27-bbe43e35a2bb",
                    "LayerId": "6ad43e98-d2b2-41d3-b1ba-3236702697a7"
                }
            ]
        },
        {
            "id": "f67afcd3-f8f6-4975-8f0d-7b633a68b951",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dde975c-329b-4440-b3c0-592cbb019daa",
            "compositeImage": {
                "id": "93a94dc5-b98c-457c-af6c-86977d388400",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f67afcd3-f8f6-4975-8f0d-7b633a68b951",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "565ce7b9-f4b6-4b37-9ff0-1440538ac4fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f67afcd3-f8f6-4975-8f0d-7b633a68b951",
                    "LayerId": "6ad43e98-d2b2-41d3-b1ba-3236702697a7"
                }
            ]
        },
        {
            "id": "d97daa69-0266-4f5c-9c4a-b5c47b91f0ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dde975c-329b-4440-b3c0-592cbb019daa",
            "compositeImage": {
                "id": "898d1d79-fbc4-4bd4-b9f0-a80b2f6a0fec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d97daa69-0266-4f5c-9c4a-b5c47b91f0ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3fae6e2-0563-49c3-a8ff-3505ef39110d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d97daa69-0266-4f5c-9c4a-b5c47b91f0ff",
                    "LayerId": "6ad43e98-d2b2-41d3-b1ba-3236702697a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6ad43e98-d2b2-41d3-b1ba-3236702697a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3dde975c-329b-4440-b3c0-592cbb019daa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}