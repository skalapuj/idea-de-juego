{
    "id": "f1439da4-cf93-42db-a110-5d37a01ca19c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "catxpos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d3bdf99f-e893-4ba9-8268-d7efc4f438cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1439da4-cf93-42db-a110-5d37a01ca19c",
            "compositeImage": {
                "id": "c8e3ca91-95ec-4180-8754-abc0360e7b8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3bdf99f-e893-4ba9-8268-d7efc4f438cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2efc71a-9b00-459b-a659-2b8cd27bc710",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3bdf99f-e893-4ba9-8268-d7efc4f438cf",
                    "LayerId": "6f11b9bc-5708-4fcb-be91-9f2a3eafb448"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6f11b9bc-5708-4fcb-be91-9f2a3eafb448",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1439da4-cf93-42db-a110-5d37a01ca19c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}