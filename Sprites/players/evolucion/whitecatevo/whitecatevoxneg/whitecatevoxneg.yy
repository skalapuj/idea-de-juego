{
    "id": "7eac1dde-94bc-4fd8-8eca-7c1be3d7805c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "whitecatevoxneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8f30d56-e5b7-4cf6-9a3f-14f7cc9b767d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7eac1dde-94bc-4fd8-8eca-7c1be3d7805c",
            "compositeImage": {
                "id": "bc2da96f-f8c4-49f4-8560-4d25656f7cb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8f30d56-e5b7-4cf6-9a3f-14f7cc9b767d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46dd5455-3ca5-4c60-8c39-838286d566e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8f30d56-e5b7-4cf6-9a3f-14f7cc9b767d",
                    "LayerId": "20ebe8a2-76c0-4bb4-958e-33606607d83e"
                }
            ]
        },
        {
            "id": "0577a4f2-6fe7-42e1-a0ac-df8d2127303d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7eac1dde-94bc-4fd8-8eca-7c1be3d7805c",
            "compositeImage": {
                "id": "9e9f7b34-2877-4893-93c4-bb65c99af6fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0577a4f2-6fe7-42e1-a0ac-df8d2127303d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7573d50-2cbf-419d-a4c6-12d1aa11e920",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0577a4f2-6fe7-42e1-a0ac-df8d2127303d",
                    "LayerId": "20ebe8a2-76c0-4bb4-958e-33606607d83e"
                }
            ]
        },
        {
            "id": "b96025a2-30e6-40e8-9daf-038fa93dc078",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7eac1dde-94bc-4fd8-8eca-7c1be3d7805c",
            "compositeImage": {
                "id": "5a8b73d6-e8c4-45c7-8423-22f9261252ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b96025a2-30e6-40e8-9daf-038fa93dc078",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d787f478-ffe2-46af-b90f-00bdd46c1855",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b96025a2-30e6-40e8-9daf-038fa93dc078",
                    "LayerId": "20ebe8a2-76c0-4bb4-958e-33606607d83e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "20ebe8a2-76c0-4bb4-958e-33606607d83e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7eac1dde-94bc-4fd8-8eca-7c1be3d7805c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}