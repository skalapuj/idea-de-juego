{
    "id": "251ccc70-4154-47b9-a6a0-c4b8c66d7bbc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "whitecatevoxpos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ebc7858e-e329-4c75-b428-ea568fa2d314",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "251ccc70-4154-47b9-a6a0-c4b8c66d7bbc",
            "compositeImage": {
                "id": "4d9d63d1-927b-47ae-b465-ba2924cc8245",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebc7858e-e329-4c75-b428-ea568fa2d314",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "453a2622-529b-4403-9f59-31729932bf74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebc7858e-e329-4c75-b428-ea568fa2d314",
                    "LayerId": "2c7bd385-c4ae-4988-b108-7f8ca30d38f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2c7bd385-c4ae-4988-b108-7f8ca30d38f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "251ccc70-4154-47b9-a6a0-c4b8c66d7bbc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}