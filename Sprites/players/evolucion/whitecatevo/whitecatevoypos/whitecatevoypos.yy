{
    "id": "f2092945-8b1f-40d7-a1e5-37c03cca3441",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "whitecatevoypos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 10,
    "bbox_right": 23,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7b40a96-40f2-4175-a330-a3ffab0502e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2092945-8b1f-40d7-a1e5-37c03cca3441",
            "compositeImage": {
                "id": "2e5167fc-6ae3-4ae6-a444-02f34ee7d1eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7b40a96-40f2-4175-a330-a3ffab0502e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3490b07e-4510-47f0-8c49-2e97f12c70ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7b40a96-40f2-4175-a330-a3ffab0502e8",
                    "LayerId": "a72b4a7d-2811-401e-b540-4c8c8377a997"
                }
            ]
        },
        {
            "id": "91d0fb1b-372c-4780-b2be-0601e837a5e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2092945-8b1f-40d7-a1e5-37c03cca3441",
            "compositeImage": {
                "id": "cf20ed45-3f6c-4ea7-be6d-31af4e9abdf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91d0fb1b-372c-4780-b2be-0601e837a5e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c35aaf4b-b329-4559-98f8-94341bed256b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91d0fb1b-372c-4780-b2be-0601e837a5e1",
                    "LayerId": "a72b4a7d-2811-401e-b540-4c8c8377a997"
                }
            ]
        },
        {
            "id": "b3d4893e-34b3-46d7-96dd-1819d84893c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2092945-8b1f-40d7-a1e5-37c03cca3441",
            "compositeImage": {
                "id": "44eb3eb6-06ef-4dcb-974b-0958269ed313",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3d4893e-34b3-46d7-96dd-1819d84893c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "116e5ba4-be92-423c-a335-224c6efbfbc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3d4893e-34b3-46d7-96dd-1819d84893c0",
                    "LayerId": "a72b4a7d-2811-401e-b540-4c8c8377a997"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a72b4a7d-2811-401e-b540-4c8c8377a997",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2092945-8b1f-40d7-a1e5-37c03cca3441",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}