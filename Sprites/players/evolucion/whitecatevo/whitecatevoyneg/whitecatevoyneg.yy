{
    "id": "9b171ed1-b43a-4d20-8704-33da3254ca2e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "whitecatevoyneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 9,
    "bbox_right": 22,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2be31f31-0900-48ad-89ee-d3feb39cb735",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b171ed1-b43a-4d20-8704-33da3254ca2e",
            "compositeImage": {
                "id": "5190132e-36ab-411f-bb47-343c6a7bc4b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2be31f31-0900-48ad-89ee-d3feb39cb735",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b81feec-be18-4f7f-a839-ed96e59a194c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2be31f31-0900-48ad-89ee-d3feb39cb735",
                    "LayerId": "7b80e242-2be3-437d-ab39-a69ac663421f"
                }
            ]
        },
        {
            "id": "c83cebb4-9741-472d-a318-5ec9bf790d1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b171ed1-b43a-4d20-8704-33da3254ca2e",
            "compositeImage": {
                "id": "0b97da42-afa8-4528-811a-4758d7eb787d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c83cebb4-9741-472d-a318-5ec9bf790d1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2388a423-e88b-4033-8ed5-bb2ccbc8b972",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c83cebb4-9741-472d-a318-5ec9bf790d1c",
                    "LayerId": "7b80e242-2be3-437d-ab39-a69ac663421f"
                }
            ]
        },
        {
            "id": "f130dd90-adec-4644-96f8-d5684c5f7919",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b171ed1-b43a-4d20-8704-33da3254ca2e",
            "compositeImage": {
                "id": "85216879-acbf-442e-a96d-bc132f592053",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f130dd90-adec-4644-96f8-d5684c5f7919",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "664c0bec-3c52-4d7f-ba2e-b5892ff98b6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f130dd90-adec-4644-96f8-d5684c5f7919",
                    "LayerId": "7b80e242-2be3-437d-ab39-a69ac663421f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7b80e242-2be3-437d-ab39-a69ac663421f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9b171ed1-b43a-4d20-8704-33da3254ca2e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}