{
    "id": "1702a29b-14a8-4020-aa9d-5aca9916cd74",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "yellowcatevoxpos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 1,
    "bbox_right": 28,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96755e70-718a-4994-a7ec-a841e65a0b21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1702a29b-14a8-4020-aa9d-5aca9916cd74",
            "compositeImage": {
                "id": "013c8133-2e94-4259-bcb3-e1daaaf606f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96755e70-718a-4994-a7ec-a841e65a0b21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4148ad01-4030-443d-9ef1-4cb19303b52b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96755e70-718a-4994-a7ec-a841e65a0b21",
                    "LayerId": "52c128be-a8e1-4a19-af43-7f232e7bff90"
                }
            ]
        },
        {
            "id": "74ee45a5-ecd4-467b-8665-3fef2d532b58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1702a29b-14a8-4020-aa9d-5aca9916cd74",
            "compositeImage": {
                "id": "5b7b2324-4e08-453d-a4c0-8bd4a7057c1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74ee45a5-ecd4-467b-8665-3fef2d532b58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a69811b5-fef9-46fb-9118-8b4175784bc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74ee45a5-ecd4-467b-8665-3fef2d532b58",
                    "LayerId": "52c128be-a8e1-4a19-af43-7f232e7bff90"
                }
            ]
        },
        {
            "id": "e7d8527c-dcb0-4835-a787-1c3e0ce39584",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1702a29b-14a8-4020-aa9d-5aca9916cd74",
            "compositeImage": {
                "id": "252a5389-27f3-4bb1-8381-084b33c2905e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7d8527c-dcb0-4835-a787-1c3e0ce39584",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc42b112-6e33-47b8-ab86-c1c82720c5e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7d8527c-dcb0-4835-a787-1c3e0ce39584",
                    "LayerId": "52c128be-a8e1-4a19-af43-7f232e7bff90"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "52c128be-a8e1-4a19-af43-7f232e7bff90",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1702a29b-14a8-4020-aa9d-5aca9916cd74",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}