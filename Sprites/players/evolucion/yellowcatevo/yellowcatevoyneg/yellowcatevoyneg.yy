{
    "id": "f968b755-f2ae-48e3-8055-1b63bad3f259",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "yellowcatevoyneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 7,
    "bbox_right": 20,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4505857c-7fb5-4773-aa5a-34573bb02c05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f968b755-f2ae-48e3-8055-1b63bad3f259",
            "compositeImage": {
                "id": "089e2b41-5c22-48d9-9a82-57b698164052",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4505857c-7fb5-4773-aa5a-34573bb02c05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e539d0cf-5f5e-4390-9039-9d19ccf3e42c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4505857c-7fb5-4773-aa5a-34573bb02c05",
                    "LayerId": "47c57171-d374-4f33-a579-ea3bb72fb693"
                }
            ]
        },
        {
            "id": "f981eeeb-e36d-470d-a085-ccd6d3b163cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f968b755-f2ae-48e3-8055-1b63bad3f259",
            "compositeImage": {
                "id": "771b255c-bca8-42b0-95b8-737f290c9425",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f981eeeb-e36d-470d-a085-ccd6d3b163cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3304fd9e-6f53-4239-a535-67ee0dc7a4f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f981eeeb-e36d-470d-a085-ccd6d3b163cb",
                    "LayerId": "47c57171-d374-4f33-a579-ea3bb72fb693"
                }
            ]
        },
        {
            "id": "c34b956c-9f85-4629-9c4f-7efef8847c5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f968b755-f2ae-48e3-8055-1b63bad3f259",
            "compositeImage": {
                "id": "6a4502b1-a292-4433-9634-572d5a7836e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c34b956c-9f85-4629-9c4f-7efef8847c5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7504f0b9-2458-4cea-b8a1-6b9d3dc57f3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c34b956c-9f85-4629-9c4f-7efef8847c5f",
                    "LayerId": "47c57171-d374-4f33-a579-ea3bb72fb693"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "47c57171-d374-4f33-a579-ea3bb72fb693",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f968b755-f2ae-48e3-8055-1b63bad3f259",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}