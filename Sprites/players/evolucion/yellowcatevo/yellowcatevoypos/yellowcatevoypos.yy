{
    "id": "f5e2f000-2021-4fd1-8225-267ff2d0ea95",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "yellowcatevoypos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 7,
    "bbox_right": 20,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1e72f71-076f-4682-8642-fa591fa36fe2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5e2f000-2021-4fd1-8225-267ff2d0ea95",
            "compositeImage": {
                "id": "6acc1451-e038-4b21-927f-578849d80983",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1e72f71-076f-4682-8642-fa591fa36fe2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5698b26e-1020-45ec-a605-cba4492af8b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1e72f71-076f-4682-8642-fa591fa36fe2",
                    "LayerId": "5ec4cf84-0089-44bf-af37-89cb4ccd607b"
                }
            ]
        },
        {
            "id": "bafaf66a-5a67-49df-9248-54491e26a8ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5e2f000-2021-4fd1-8225-267ff2d0ea95",
            "compositeImage": {
                "id": "0e83a65d-fb29-4c79-8a0c-a7e5c053eac9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bafaf66a-5a67-49df-9248-54491e26a8ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c64bcc1c-061d-4462-9434-164c83fa9d1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bafaf66a-5a67-49df-9248-54491e26a8ca",
                    "LayerId": "5ec4cf84-0089-44bf-af37-89cb4ccd607b"
                }
            ]
        },
        {
            "id": "ce120bc4-9382-4320-a04a-1a98b4b3d52e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5e2f000-2021-4fd1-8225-267ff2d0ea95",
            "compositeImage": {
                "id": "d1718e70-0643-461c-93ca-6dbf99d19263",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce120bc4-9382-4320-a04a-1a98b4b3d52e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62820193-fea4-42c9-bcf4-9c0b00fc61ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce120bc4-9382-4320-a04a-1a98b4b3d52e",
                    "LayerId": "5ec4cf84-0089-44bf-af37-89cb4ccd607b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5ec4cf84-0089-44bf-af37-89cb4ccd607b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5e2f000-2021-4fd1-8225-267ff2d0ea95",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}