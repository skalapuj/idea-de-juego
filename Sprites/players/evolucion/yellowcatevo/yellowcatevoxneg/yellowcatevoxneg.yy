{
    "id": "7f109357-3b51-4694-b87c-30ef7477bd41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "yellowcatevoxneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e0306ae-f988-4213-8453-787b7a0fb950",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f109357-3b51-4694-b87c-30ef7477bd41",
            "compositeImage": {
                "id": "81bcd630-8ba2-4152-a9a0-e3835ce3ee44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e0306ae-f988-4213-8453-787b7a0fb950",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2511672-415a-45ce-a6c0-93d9c1578ba7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e0306ae-f988-4213-8453-787b7a0fb950",
                    "LayerId": "66e5ca02-7142-40fa-8f24-57f5abd5f120"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "66e5ca02-7142-40fa-8f24-57f5abd5f120",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f109357-3b51-4694-b87c-30ef7477bd41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}