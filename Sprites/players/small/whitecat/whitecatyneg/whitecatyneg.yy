{
    "id": "a6c8b52d-6590-42fe-b0e2-b3a7afc689ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "whitecatyneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 7,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7baac316-9cfd-4283-89d4-34f0193e08ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6c8b52d-6590-42fe-b0e2-b3a7afc689ce",
            "compositeImage": {
                "id": "29af03db-00fe-4392-97c7-3b9032a750cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7baac316-9cfd-4283-89d4-34f0193e08ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08f81232-bece-4f76-abb5-84b4be51fb48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7baac316-9cfd-4283-89d4-34f0193e08ef",
                    "LayerId": "2210dd03-29d1-4d94-9f21-cb314254d428"
                }
            ]
        },
        {
            "id": "907aa836-eccd-4f40-8d6c-277c3b20f031",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6c8b52d-6590-42fe-b0e2-b3a7afc689ce",
            "compositeImage": {
                "id": "eeecd6e2-3d04-4a0c-93f0-5d44e1d0a793",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "907aa836-eccd-4f40-8d6c-277c3b20f031",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe8cea19-9143-43d1-98d0-54af61546663",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "907aa836-eccd-4f40-8d6c-277c3b20f031",
                    "LayerId": "2210dd03-29d1-4d94-9f21-cb314254d428"
                }
            ]
        },
        {
            "id": "2736f94d-589a-4e0b-8ad0-16c9f67e07a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6c8b52d-6590-42fe-b0e2-b3a7afc689ce",
            "compositeImage": {
                "id": "b79fba16-a886-4d07-9b5c-e9a6f2aa607e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2736f94d-589a-4e0b-8ad0-16c9f67e07a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6edfb7e7-ba9a-4080-9566-f5fd83e7ec58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2736f94d-589a-4e0b-8ad0-16c9f67e07a2",
                    "LayerId": "2210dd03-29d1-4d94-9f21-cb314254d428"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2210dd03-29d1-4d94-9f21-cb314254d428",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6c8b52d-6590-42fe-b0e2-b3a7afc689ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 0,
    "yorig": 0
}