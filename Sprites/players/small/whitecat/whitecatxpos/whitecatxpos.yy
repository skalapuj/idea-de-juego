{
    "id": "8cee2144-237c-4fc4-b18b-cf633b175800",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "whitecatxpos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "65b336ee-720a-46ec-a344-dfb2efdb75aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cee2144-237c-4fc4-b18b-cf633b175800",
            "compositeImage": {
                "id": "d499d9f0-e328-4750-b374-7bcab77682d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65b336ee-720a-46ec-a344-dfb2efdb75aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19301f34-c588-4333-bc19-c7366889fae1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65b336ee-720a-46ec-a344-dfb2efdb75aa",
                    "LayerId": "73c36418-0846-46a1-bcf0-3e3a253374a8"
                }
            ]
        },
        {
            "id": "a28807c8-d9cc-4035-8d6b-1790fdae7496",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cee2144-237c-4fc4-b18b-cf633b175800",
            "compositeImage": {
                "id": "03204faa-2ede-4902-8f0c-eaa8ff020955",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a28807c8-d9cc-4035-8d6b-1790fdae7496",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "347368e4-33eb-4af9-a454-77309d9736aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a28807c8-d9cc-4035-8d6b-1790fdae7496",
                    "LayerId": "73c36418-0846-46a1-bcf0-3e3a253374a8"
                }
            ]
        },
        {
            "id": "631be7aa-5b1c-448a-b5c3-ad08e91180bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cee2144-237c-4fc4-b18b-cf633b175800",
            "compositeImage": {
                "id": "d519957c-2e12-40d4-8a4f-7b94cc37e308",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "631be7aa-5b1c-448a-b5c3-ad08e91180bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6910fae6-8134-4a7f-98bb-f804af892d95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "631be7aa-5b1c-448a-b5c3-ad08e91180bf",
                    "LayerId": "73c36418-0846-46a1-bcf0-3e3a253374a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "73c36418-0846-46a1-bcf0-3e3a253374a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8cee2144-237c-4fc4-b18b-cf633b175800",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 0,
    "yorig": 0
}