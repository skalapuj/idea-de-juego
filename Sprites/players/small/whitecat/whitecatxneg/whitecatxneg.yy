{
    "id": "758f7be1-a111-45e8-887d-ef52c1c6cccc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "whitecatxneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ebf28502-daf6-4ace-a7d9-d45202013d76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "758f7be1-a111-45e8-887d-ef52c1c6cccc",
            "compositeImage": {
                "id": "3671db76-6e08-4869-b993-545194dda9c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebf28502-daf6-4ace-a7d9-d45202013d76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61471689-64ee-4297-8a24-1e2f8fe70767",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebf28502-daf6-4ace-a7d9-d45202013d76",
                    "LayerId": "aefd1624-52c0-4982-8296-20d3975d2afc"
                }
            ]
        },
        {
            "id": "a13ab61f-b6ac-46c1-b1f4-2ca9e89ac51c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "758f7be1-a111-45e8-887d-ef52c1c6cccc",
            "compositeImage": {
                "id": "832b99f8-6135-4345-93db-c77d780d743d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a13ab61f-b6ac-46c1-b1f4-2ca9e89ac51c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acbb3aec-60da-45df-8c10-b6011bf1d027",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a13ab61f-b6ac-46c1-b1f4-2ca9e89ac51c",
                    "LayerId": "aefd1624-52c0-4982-8296-20d3975d2afc"
                }
            ]
        },
        {
            "id": "bcbe99ed-73c9-4287-80c9-dcd71ad9078c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "758f7be1-a111-45e8-887d-ef52c1c6cccc",
            "compositeImage": {
                "id": "f85edebf-e379-436c-9802-5477735ae059",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcbe99ed-73c9-4287-80c9-dcd71ad9078c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4289db1-f763-425c-a431-4787a45008f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcbe99ed-73c9-4287-80c9-dcd71ad9078c",
                    "LayerId": "aefd1624-52c0-4982-8296-20d3975d2afc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "aefd1624-52c0-4982-8296-20d3975d2afc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "758f7be1-a111-45e8-887d-ef52c1c6cccc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 0,
    "yorig": 0
}