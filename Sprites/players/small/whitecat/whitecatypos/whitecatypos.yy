{
    "id": "78f296de-3abe-4016-ba83-388f91dd5950",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "whitecatypos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 10,
    "bbox_right": 25,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "164b372b-1cdb-4ce8-97e0-f8167f200e08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f296de-3abe-4016-ba83-388f91dd5950",
            "compositeImage": {
                "id": "67ca1a72-a838-46e5-9eec-dd782828ec6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "164b372b-1cdb-4ce8-97e0-f8167f200e08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e45fc1e0-3365-4439-8b15-de852075165a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "164b372b-1cdb-4ce8-97e0-f8167f200e08",
                    "LayerId": "c023d981-5b91-4452-8757-f9352b9b8a66"
                }
            ]
        },
        {
            "id": "5338f7bf-8959-488e-b419-2d5cf11ad82b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f296de-3abe-4016-ba83-388f91dd5950",
            "compositeImage": {
                "id": "2df9a11a-c4df-4259-babd-50307c5d2937",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5338f7bf-8959-488e-b419-2d5cf11ad82b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13a3ec55-a3b1-464a-8df6-a62bb0962e4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5338f7bf-8959-488e-b419-2d5cf11ad82b",
                    "LayerId": "c023d981-5b91-4452-8757-f9352b9b8a66"
                }
            ]
        },
        {
            "id": "c6c1fa9d-f868-4442-8370-10fc0b695222",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f296de-3abe-4016-ba83-388f91dd5950",
            "compositeImage": {
                "id": "0b41ea3f-9b5f-45f3-a0af-c305c7ef0454",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6c1fa9d-f868-4442-8370-10fc0b695222",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5b62a24-bf05-41c7-9a21-e28f920fdba2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6c1fa9d-f868-4442-8370-10fc0b695222",
                    "LayerId": "c023d981-5b91-4452-8757-f9352b9b8a66"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c023d981-5b91-4452-8757-f9352b9b8a66",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "78f296de-3abe-4016-ba83-388f91dd5950",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 0,
    "yorig": 0
}