{
    "id": "3aa6d583-47db-482a-ad56-9148cad12385",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "yelloscatyneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 8,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "538d61e5-c50b-4e55-833e-a38f60f03f95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3aa6d583-47db-482a-ad56-9148cad12385",
            "compositeImage": {
                "id": "86b524fb-d29c-4fa5-a237-87c2965f2aee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "538d61e5-c50b-4e55-833e-a38f60f03f95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "087ba086-e26f-4955-90e5-4cd6a65f70eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "538d61e5-c50b-4e55-833e-a38f60f03f95",
                    "LayerId": "62bfa20f-f43e-401a-8682-9b46db85bc26"
                }
            ]
        },
        {
            "id": "4c4c66b7-d36a-465c-a228-0c6391018767",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3aa6d583-47db-482a-ad56-9148cad12385",
            "compositeImage": {
                "id": "36e8347f-6988-442f-9a45-8d700cc0f375",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c4c66b7-d36a-465c-a228-0c6391018767",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d82057ab-36b2-4d3b-a19c-9bda10e5b0ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c4c66b7-d36a-465c-a228-0c6391018767",
                    "LayerId": "62bfa20f-f43e-401a-8682-9b46db85bc26"
                }
            ]
        },
        {
            "id": "858ce421-143d-4e9f-8ee3-f49d6c802dbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3aa6d583-47db-482a-ad56-9148cad12385",
            "compositeImage": {
                "id": "f6e7a52d-b5c1-4d64-a775-dbbd7cb8bcf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "858ce421-143d-4e9f-8ee3-f49d6c802dbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87d9b6d3-0411-40e2-8700-f2fcbbb4ab1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "858ce421-143d-4e9f-8ee3-f49d6c802dbc",
                    "LayerId": "62bfa20f-f43e-401a-8682-9b46db85bc26"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "62bfa20f-f43e-401a-8682-9b46db85bc26",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3aa6d583-47db-482a-ad56-9148cad12385",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 0,
    "yorig": 0
}