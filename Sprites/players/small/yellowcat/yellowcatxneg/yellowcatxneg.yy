{
    "id": "072b006a-5733-4117-a16e-d517f4886eea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "yellowcatxneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0b7476ad-5b31-4928-b934-b31b59af9fcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "072b006a-5733-4117-a16e-d517f4886eea",
            "compositeImage": {
                "id": "6434040c-d1cd-4ddc-b268-549b2a955a75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b7476ad-5b31-4928-b934-b31b59af9fcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dfa1b01-7777-48a3-a7ce-34eb98a9cd8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b7476ad-5b31-4928-b934-b31b59af9fcc",
                    "LayerId": "58363255-183e-45f2-ab27-64225e88f639"
                }
            ]
        },
        {
            "id": "09938075-0926-4d65-8151-3d00e4936333",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "072b006a-5733-4117-a16e-d517f4886eea",
            "compositeImage": {
                "id": "031993d2-0037-4006-9901-72d74428e8cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09938075-0926-4d65-8151-3d00e4936333",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58bf1287-1b14-412e-a918-995cceebb9ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09938075-0926-4d65-8151-3d00e4936333",
                    "LayerId": "58363255-183e-45f2-ab27-64225e88f639"
                }
            ]
        },
        {
            "id": "219e045a-c282-4517-a463-469d3fe60103",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "072b006a-5733-4117-a16e-d517f4886eea",
            "compositeImage": {
                "id": "0d8b18b5-780e-4541-b3b6-261682853747",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "219e045a-c282-4517-a463-469d3fe60103",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "567cb3c5-6505-40a6-94f7-3a1a529954e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "219e045a-c282-4517-a463-469d3fe60103",
                    "LayerId": "58363255-183e-45f2-ab27-64225e88f639"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "58363255-183e-45f2-ab27-64225e88f639",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "072b006a-5733-4117-a16e-d517f4886eea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 0,
    "yorig": 0
}