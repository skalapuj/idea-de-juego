{
    "id": "24a314e3-604f-4bee-ba6b-e2dcd6c23902",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "yellowcatxpos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2f1e00d3-c273-47e5-a0a7-837b6f06e2e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24a314e3-604f-4bee-ba6b-e2dcd6c23902",
            "compositeImage": {
                "id": "cbc9b0c0-ce1d-41ab-a6c2-56cc08a95dc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f1e00d3-c273-47e5-a0a7-837b6f06e2e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2eefe9d-445a-4a38-b43b-cfd9fd6cf445",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f1e00d3-c273-47e5-a0a7-837b6f06e2e6",
                    "LayerId": "1508ff88-aa60-4e7e-af29-598c3bd0b61c"
                }
            ]
        },
        {
            "id": "b404fa2e-1647-4a1d-82fe-507d086decee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24a314e3-604f-4bee-ba6b-e2dcd6c23902",
            "compositeImage": {
                "id": "9a0104c5-db23-44f1-85cf-2e925d2e5fc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b404fa2e-1647-4a1d-82fe-507d086decee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a736765-17b5-4c47-816b-e8db2bd3a5d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b404fa2e-1647-4a1d-82fe-507d086decee",
                    "LayerId": "1508ff88-aa60-4e7e-af29-598c3bd0b61c"
                }
            ]
        },
        {
            "id": "10486e7f-4e14-4f79-9349-e5b9de2b6bef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24a314e3-604f-4bee-ba6b-e2dcd6c23902",
            "compositeImage": {
                "id": "be8062d9-d4cf-4cc2-ac9f-3dd7bc7eb9f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10486e7f-4e14-4f79-9349-e5b9de2b6bef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c120e542-66be-43f5-8ed3-0d3370174590",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10486e7f-4e14-4f79-9349-e5b9de2b6bef",
                    "LayerId": "1508ff88-aa60-4e7e-af29-598c3bd0b61c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "1508ff88-aa60-4e7e-af29-598c3bd0b61c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "24a314e3-604f-4bee-ba6b-e2dcd6c23902",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 0,
    "yorig": 0
}