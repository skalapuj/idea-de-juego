{
    "id": "db241b1b-bd6e-49be-b786-e5865e106b67",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "yellowcatypos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 7,
    "bbox_right": 22,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93af87f2-5fcb-43f7-95ff-574d0a76be8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db241b1b-bd6e-49be-b786-e5865e106b67",
            "compositeImage": {
                "id": "443978fa-297e-4f48-bd5c-906cd443bdf2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93af87f2-5fcb-43f7-95ff-574d0a76be8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "879d3283-410b-4584-bae2-24dbcc02a1e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93af87f2-5fcb-43f7-95ff-574d0a76be8b",
                    "LayerId": "57ecac1e-c1fc-49fe-89b1-dc30633b3c73"
                }
            ]
        },
        {
            "id": "106d69ba-b644-4eae-a0c4-76bb96c82be3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db241b1b-bd6e-49be-b786-e5865e106b67",
            "compositeImage": {
                "id": "865d7c5d-4c29-4c21-9bdf-c44b34704940",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "106d69ba-b644-4eae-a0c4-76bb96c82be3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7afd71b2-d935-489a-8fe1-62a600bc6f13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "106d69ba-b644-4eae-a0c4-76bb96c82be3",
                    "LayerId": "57ecac1e-c1fc-49fe-89b1-dc30633b3c73"
                }
            ]
        },
        {
            "id": "204f0709-5ba5-4efe-8b7d-53fa1a91254c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db241b1b-bd6e-49be-b786-e5865e106b67",
            "compositeImage": {
                "id": "7b110ac6-f937-4520-b10d-a0b7397e7e11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "204f0709-5ba5-4efe-8b7d-53fa1a91254c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8085e22-ee6b-4313-b062-13ef3eb3d9a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "204f0709-5ba5-4efe-8b7d-53fa1a91254c",
                    "LayerId": "57ecac1e-c1fc-49fe-89b1-dc30633b3c73"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "57ecac1e-c1fc-49fe-89b1-dc30633b3c73",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db241b1b-bd6e-49be-b786-e5865e106b67",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 0,
    "yorig": 0
}