{
    "id": "2bcabd87-03cf-415b-97db-53533c5ca272",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bigpigxneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2dcc95c6-7c55-47c3-a881-2cf6b02f00f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bcabd87-03cf-415b-97db-53533c5ca272",
            "compositeImage": {
                "id": "7e333b02-bc8f-4d14-a37a-d741cb29bf09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dcc95c6-7c55-47c3-a881-2cf6b02f00f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fdd50af-659c-4a03-bd26-8346d6f6812c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dcc95c6-7c55-47c3-a881-2cf6b02f00f0",
                    "LayerId": "7873f91f-13b5-4394-b8d3-96e09f52f013"
                }
            ]
        },
        {
            "id": "61656238-c81c-45c4-831c-a77c1a37f71f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bcabd87-03cf-415b-97db-53533c5ca272",
            "compositeImage": {
                "id": "075ec73a-ba89-4483-b6f4-58ae7a09c85b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61656238-c81c-45c4-831c-a77c1a37f71f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f55e34d-a789-4342-a882-f495b66d7ec7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61656238-c81c-45c4-831c-a77c1a37f71f",
                    "LayerId": "7873f91f-13b5-4394-b8d3-96e09f52f013"
                }
            ]
        },
        {
            "id": "a6c52ba7-9ab9-44f5-b4f5-557d15bedcdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bcabd87-03cf-415b-97db-53533c5ca272",
            "compositeImage": {
                "id": "90f53598-61d2-4fdf-aac1-457468e8e0eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6c52ba7-9ab9-44f5-b4f5-557d15bedcdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a86b44f9-5676-4e21-a714-e1ec93598cad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6c52ba7-9ab9-44f5-b4f5-557d15bedcdb",
                    "LayerId": "7873f91f-13b5-4394-b8d3-96e09f52f013"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "7873f91f-13b5-4394-b8d3-96e09f52f013",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2bcabd87-03cf-415b-97db-53533c5ca272",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}