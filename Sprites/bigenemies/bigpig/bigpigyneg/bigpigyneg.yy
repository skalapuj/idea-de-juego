{
    "id": "169b5622-0932-495a-bc76-552562b20067",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bigpigyneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d8aac17-9de7-4659-ba72-7369d8f008a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "169b5622-0932-495a-bc76-552562b20067",
            "compositeImage": {
                "id": "647a2bac-e73d-4d7d-9492-951c062466b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d8aac17-9de7-4659-ba72-7369d8f008a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b63ff81f-8cd8-42b9-89ae-1f2b64ec62d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d8aac17-9de7-4659-ba72-7369d8f008a8",
                    "LayerId": "081ec3a3-c41f-4c71-8f6a-7c590b632b0c"
                }
            ]
        },
        {
            "id": "35ea8423-a2c2-4c13-9851-1ef8de2d04cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "169b5622-0932-495a-bc76-552562b20067",
            "compositeImage": {
                "id": "68878048-2f1e-47da-9785-1a59a035cd07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35ea8423-a2c2-4c13-9851-1ef8de2d04cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb748283-2e12-403e-99d6-b6e89b1d7cd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35ea8423-a2c2-4c13-9851-1ef8de2d04cf",
                    "LayerId": "081ec3a3-c41f-4c71-8f6a-7c590b632b0c"
                }
            ]
        },
        {
            "id": "34544a2b-04f0-4180-a5e5-c5f3b68c4411",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "169b5622-0932-495a-bc76-552562b20067",
            "compositeImage": {
                "id": "45d64065-695e-4730-89f1-8fe06243bfe0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34544a2b-04f0-4180-a5e5-c5f3b68c4411",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "052e3eef-1fb5-4633-87a0-4e50333086b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34544a2b-04f0-4180-a5e5-c5f3b68c4411",
                    "LayerId": "081ec3a3-c41f-4c71-8f6a-7c590b632b0c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "081ec3a3-c41f-4c71-8f6a-7c590b632b0c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "169b5622-0932-495a-bc76-552562b20067",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}