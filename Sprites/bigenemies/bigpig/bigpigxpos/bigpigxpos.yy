{
    "id": "cdbcdc01-caee-4c91-a947-0df4aa58dffd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bigpigxpos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db0f6cd3-8e40-4b22-8ed7-7a4ee691937e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cdbcdc01-caee-4c91-a947-0df4aa58dffd",
            "compositeImage": {
                "id": "73977754-b094-471b-86ea-757b30521ccb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db0f6cd3-8e40-4b22-8ed7-7a4ee691937e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8d65983-2a29-4c1b-9174-8675f85d1635",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db0f6cd3-8e40-4b22-8ed7-7a4ee691937e",
                    "LayerId": "6f4a2022-690d-40fb-954a-e5e0d767ed64"
                }
            ]
        },
        {
            "id": "e1337da2-93ef-425e-a1d7-4d1afc52bf10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cdbcdc01-caee-4c91-a947-0df4aa58dffd",
            "compositeImage": {
                "id": "31a4d78d-b77f-4aa2-92f9-eea7f008182a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1337da2-93ef-425e-a1d7-4d1afc52bf10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b797929-828c-4b4a-b1c0-bb3563ec3f93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1337da2-93ef-425e-a1d7-4d1afc52bf10",
                    "LayerId": "6f4a2022-690d-40fb-954a-e5e0d767ed64"
                }
            ]
        },
        {
            "id": "aa4b24b4-5b8a-490b-9954-1b128c03eedf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cdbcdc01-caee-4c91-a947-0df4aa58dffd",
            "compositeImage": {
                "id": "48a567ab-ef8c-4a0a-9b20-bd8edd0f54be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa4b24b4-5b8a-490b-9954-1b128c03eedf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "802ef153-34de-4273-8b13-9220c1cef009",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa4b24b4-5b8a-490b-9954-1b128c03eedf",
                    "LayerId": "6f4a2022-690d-40fb-954a-e5e0d767ed64"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "6f4a2022-690d-40fb-954a-e5e0d767ed64",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cdbcdc01-caee-4c91-a947-0df4aa58dffd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}