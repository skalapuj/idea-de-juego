{
    "id": "75d86bcf-3ff8-4498-aa6d-e46ca685fb56",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bigpigypos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 1,
    "bbox_right": 26,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05eb940b-31f7-4ae9-a1de-e37cb79354ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d86bcf-3ff8-4498-aa6d-e46ca685fb56",
            "compositeImage": {
                "id": "b885a039-7a63-4386-bf53-497f2a331321",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05eb940b-31f7-4ae9-a1de-e37cb79354ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "876c7a45-a2b4-450c-b148-b50239be95f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05eb940b-31f7-4ae9-a1de-e37cb79354ec",
                    "LayerId": "dcc15877-bfdc-4437-a38a-3b28723f1bca"
                }
            ]
        },
        {
            "id": "33bbd79b-cd4a-4c88-a053-14ffca58f10f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d86bcf-3ff8-4498-aa6d-e46ca685fb56",
            "compositeImage": {
                "id": "0b1bf398-85ac-41f9-9987-9f2c1e9c2ad2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33bbd79b-cd4a-4c88-a053-14ffca58f10f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c090a40-9f60-4b40-a5e3-75aa083d652d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33bbd79b-cd4a-4c88-a053-14ffca58f10f",
                    "LayerId": "dcc15877-bfdc-4437-a38a-3b28723f1bca"
                }
            ]
        },
        {
            "id": "52b02b8c-00d7-48b3-8d43-cf0e47e10019",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d86bcf-3ff8-4498-aa6d-e46ca685fb56",
            "compositeImage": {
                "id": "5769411d-8732-43a2-afc8-1018301cd3e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52b02b8c-00d7-48b3-8d43-cf0e47e10019",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "987287b8-1833-4299-b6aa-c5e1639d0c5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52b02b8c-00d7-48b3-8d43-cf0e47e10019",
                    "LayerId": "dcc15877-bfdc-4437-a38a-3b28723f1bca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "dcc15877-bfdc-4437-a38a-3b28723f1bca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75d86bcf-3ff8-4498-aa6d-e46ca685fb56",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 23,
    "yorig": 22
}