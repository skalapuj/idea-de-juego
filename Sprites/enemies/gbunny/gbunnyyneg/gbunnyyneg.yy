{
    "id": "aaf2fbcc-f93e-409a-9b26-91bf6f1b6680",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gbunnyyneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 14,
    "bbox_right": 32,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c47e7a3-3a40-405b-a050-ac2374804884",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aaf2fbcc-f93e-409a-9b26-91bf6f1b6680",
            "compositeImage": {
                "id": "0face9eb-3b90-42ad-96ea-5ccf720ec156",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c47e7a3-3a40-405b-a050-ac2374804884",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c99b8986-bde3-4e98-be2a-35f60ee815bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c47e7a3-3a40-405b-a050-ac2374804884",
                    "LayerId": "610957a0-e0c4-4149-9f11-66cc8284051f"
                }
            ]
        },
        {
            "id": "ea7faf19-47d0-4553-abc8-24efb274ec73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aaf2fbcc-f93e-409a-9b26-91bf6f1b6680",
            "compositeImage": {
                "id": "003bcc80-c697-47ff-9555-ed586ad50408",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea7faf19-47d0-4553-abc8-24efb274ec73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "374340f0-7c53-45e7-8cdd-f3753aa67277",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea7faf19-47d0-4553-abc8-24efb274ec73",
                    "LayerId": "610957a0-e0c4-4149-9f11-66cc8284051f"
                }
            ]
        },
        {
            "id": "54ac0c07-15e2-4d26-99e9-c9366ec04fa4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aaf2fbcc-f93e-409a-9b26-91bf6f1b6680",
            "compositeImage": {
                "id": "cef620a4-0afc-4fd1-bdc7-910d397bb474",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54ac0c07-15e2-4d26-99e9-c9366ec04fa4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f181151e-1bf6-4c76-8e47-0da1dfd46e72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54ac0c07-15e2-4d26-99e9-c9366ec04fa4",
                    "LayerId": "610957a0-e0c4-4149-9f11-66cc8284051f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "610957a0-e0c4-4149-9f11-66cc8284051f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aaf2fbcc-f93e-409a-9b26-91bf6f1b6680",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": -301,
    "yorig": -116
}