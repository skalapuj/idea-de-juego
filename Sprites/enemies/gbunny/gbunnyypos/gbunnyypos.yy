{
    "id": "f0faa372-38d1-4cc5-845a-565b4b334561",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gbunnyypos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 14,
    "bbox_right": 32,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4aee257-62c8-4639-a601-7801d64e1f9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0faa372-38d1-4cc5-845a-565b4b334561",
            "compositeImage": {
                "id": "133f433d-7017-4877-89f3-8e33b8bd0f06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4aee257-62c8-4639-a601-7801d64e1f9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b83aaf69-64f6-4d9e-9430-50ce1523b92d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4aee257-62c8-4639-a601-7801d64e1f9b",
                    "LayerId": "14c9177d-b6c9-45a3-9824-242f81e497e0"
                }
            ]
        },
        {
            "id": "c9ddab48-c96c-4329-884a-24b52f84ca3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0faa372-38d1-4cc5-845a-565b4b334561",
            "compositeImage": {
                "id": "5c54a505-7ba8-40aa-ab7f-bc87e17da308",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9ddab48-c96c-4329-884a-24b52f84ca3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e5072f0-acfe-4273-a28d-46a0eb2d4564",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9ddab48-c96c-4329-884a-24b52f84ca3a",
                    "LayerId": "14c9177d-b6c9-45a3-9824-242f81e497e0"
                }
            ]
        },
        {
            "id": "065bbd12-e9b6-46dc-ad7e-b52a570f2dbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0faa372-38d1-4cc5-845a-565b4b334561",
            "compositeImage": {
                "id": "f3b18c95-835d-41b8-b9dd-3c90f6a0fc67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "065bbd12-e9b6-46dc-ad7e-b52a570f2dbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d661ea8-3400-41b0-a5fa-63059e8df5c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "065bbd12-e9b6-46dc-ad7e-b52a570f2dbf",
                    "LayerId": "14c9177d-b6c9-45a3-9824-242f81e497e0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "14c9177d-b6c9-45a3-9824-242f81e497e0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f0faa372-38d1-4cc5-845a-565b4b334561",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}