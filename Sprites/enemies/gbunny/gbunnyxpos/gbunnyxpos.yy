{
    "id": "4c74726b-93a5-4103-af2a-15e8434c1a24",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gbunnyxpos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 9,
    "bbox_right": 37,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "42f848ed-7fcf-4469-8741-396ccd723ae3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c74726b-93a5-4103-af2a-15e8434c1a24",
            "compositeImage": {
                "id": "e40bb0ab-2024-4c29-91a9-a0f9f15ac413",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42f848ed-7fcf-4469-8741-396ccd723ae3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06b1b188-5eaa-4cb9-b91b-f8408f0704b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42f848ed-7fcf-4469-8741-396ccd723ae3",
                    "LayerId": "cd7b0e5b-a81e-4f8d-91a4-7dabf0197a93"
                }
            ]
        },
        {
            "id": "48417cbe-4339-4b19-9d4e-2a0ed3a4f6f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c74726b-93a5-4103-af2a-15e8434c1a24",
            "compositeImage": {
                "id": "ca3841c8-3d66-4bde-a077-7365ebf7d408",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48417cbe-4339-4b19-9d4e-2a0ed3a4f6f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93845954-e2b1-4d19-81d3-7ee8776e5e65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48417cbe-4339-4b19-9d4e-2a0ed3a4f6f6",
                    "LayerId": "cd7b0e5b-a81e-4f8d-91a4-7dabf0197a93"
                }
            ]
        },
        {
            "id": "7f22eead-3580-4333-ae86-923c6286bb0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c74726b-93a5-4103-af2a-15e8434c1a24",
            "compositeImage": {
                "id": "a03b0db5-7846-4275-a9a3-75aefb8c2d69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f22eead-3580-4333-ae86-923c6286bb0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "205fd439-9bce-47ec-941c-e94bbef18a50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f22eead-3580-4333-ae86-923c6286bb0b",
                    "LayerId": "cd7b0e5b-a81e-4f8d-91a4-7dabf0197a93"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "cd7b0e5b-a81e-4f8d-91a4-7dabf0197a93",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c74726b-93a5-4103-af2a-15e8434c1a24",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 41,
    "yorig": 33
}