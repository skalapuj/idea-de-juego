{
    "id": "2bdd0b8d-90e0-40b0-9e94-1e6f2ba1bd77",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gbunnyxneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 12,
    "bbox_right": 40,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4aceb93c-b5c8-47fb-ad6f-1964ada894a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bdd0b8d-90e0-40b0-9e94-1e6f2ba1bd77",
            "compositeImage": {
                "id": "dfbc297c-82b3-43da-8495-aa20d9d354ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4aceb93c-b5c8-47fb-ad6f-1964ada894a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b348f734-8dea-4c10-a696-b506aea70a7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4aceb93c-b5c8-47fb-ad6f-1964ada894a3",
                    "LayerId": "cf88ab62-a054-4589-9a63-669856d6b9c8"
                }
            ]
        },
        {
            "id": "493d8bf2-6750-4eb3-85fb-a68e737a781d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bdd0b8d-90e0-40b0-9e94-1e6f2ba1bd77",
            "compositeImage": {
                "id": "c43808fc-5f47-4ccb-962d-d106ddab98a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "493d8bf2-6750-4eb3-85fb-a68e737a781d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9c39133-a634-403a-b281-094b7fa43936",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "493d8bf2-6750-4eb3-85fb-a68e737a781d",
                    "LayerId": "cf88ab62-a054-4589-9a63-669856d6b9c8"
                }
            ]
        },
        {
            "id": "051d3675-ebef-44ed-8720-00a6c2d2798f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bdd0b8d-90e0-40b0-9e94-1e6f2ba1bd77",
            "compositeImage": {
                "id": "53821a85-9782-4095-a1a8-525b1e4edbbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "051d3675-ebef-44ed-8720-00a6c2d2798f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b70a7fc-7d3e-42ee-8722-11a5182f2583",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "051d3675-ebef-44ed-8720-00a6c2d2798f",
                    "LayerId": "cf88ab62-a054-4589-9a63-669856d6b9c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "cf88ab62-a054-4589-9a63-669856d6b9c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2bdd0b8d-90e0-40b0-9e94-1e6f2ba1bd77",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}