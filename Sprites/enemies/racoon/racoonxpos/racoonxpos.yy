{
    "id": "6048dd08-556a-4711-a60c-8d05cc678739",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "racoonxpos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 1,
    "bbox_right": 27,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f16eb7a1-52ff-4d02-a912-58a87889c66b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6048dd08-556a-4711-a60c-8d05cc678739",
            "compositeImage": {
                "id": "5eff2616-28c9-4b10-ba1f-ce44c8e64922",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f16eb7a1-52ff-4d02-a912-58a87889c66b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8814513-12b9-4545-81fa-4efc98968299",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f16eb7a1-52ff-4d02-a912-58a87889c66b",
                    "LayerId": "2746af6d-7081-4787-a673-72c080d6780c"
                }
            ]
        },
        {
            "id": "8d22e25b-f7c4-4dc7-a55f-2761f5f01d54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6048dd08-556a-4711-a60c-8d05cc678739",
            "compositeImage": {
                "id": "cd8b5328-d0e8-4737-a79b-41803d73004d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d22e25b-f7c4-4dc7-a55f-2761f5f01d54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d7d7883-90b9-4c0d-aa39-0d0c7b4bca2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d22e25b-f7c4-4dc7-a55f-2761f5f01d54",
                    "LayerId": "2746af6d-7081-4787-a673-72c080d6780c"
                }
            ]
        },
        {
            "id": "d008f023-b5c3-4ff2-9d43-429223a31231",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6048dd08-556a-4711-a60c-8d05cc678739",
            "compositeImage": {
                "id": "a40d82bf-24e2-48fa-b323-9ee5e9d796b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d008f023-b5c3-4ff2-9d43-429223a31231",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "061b39c3-541d-46b8-aac0-7b8178e75312",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d008f023-b5c3-4ff2-9d43-429223a31231",
                    "LayerId": "2746af6d-7081-4787-a673-72c080d6780c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "2746af6d-7081-4787-a673-72c080d6780c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6048dd08-556a-4711-a60c-8d05cc678739",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}