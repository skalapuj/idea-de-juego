{
    "id": "73a7cf7a-ab4f-4090-9ec2-05ed0167e688",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "racoonxneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 27,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d205ad2-4c0b-49f0-9c77-f3330268e0df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73a7cf7a-ab4f-4090-9ec2-05ed0167e688",
            "compositeImage": {
                "id": "c42b178a-1477-4443-89e8-65ed9ba6fb8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d205ad2-4c0b-49f0-9c77-f3330268e0df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3ffa935-26b3-4665-8ff8-48b957be4dee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d205ad2-4c0b-49f0-9c77-f3330268e0df",
                    "LayerId": "9aa66b65-ce48-46a1-9ebe-863b9b577860"
                }
            ]
        },
        {
            "id": "fa2ef6a9-98f0-4170-8d25-fb2e6f3cc4f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73a7cf7a-ab4f-4090-9ec2-05ed0167e688",
            "compositeImage": {
                "id": "e5739248-76df-40c3-af1f-36fdc28f1f86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa2ef6a9-98f0-4170-8d25-fb2e6f3cc4f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a3e5885-cd99-4ae0-827c-7826beeee903",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa2ef6a9-98f0-4170-8d25-fb2e6f3cc4f6",
                    "LayerId": "9aa66b65-ce48-46a1-9ebe-863b9b577860"
                }
            ]
        },
        {
            "id": "1e089a93-fb31-4411-8026-a8b03e0e6acf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73a7cf7a-ab4f-4090-9ec2-05ed0167e688",
            "compositeImage": {
                "id": "6b713c90-4e21-4d95-b385-c03ac0a0a096",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e089a93-fb31-4411-8026-a8b03e0e6acf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d81bafd8-377c-4f9f-a79a-2ba4f8c9ff2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e089a93-fb31-4411-8026-a8b03e0e6acf",
                    "LayerId": "9aa66b65-ce48-46a1-9ebe-863b9b577860"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "9aa66b65-ce48-46a1-9ebe-863b9b577860",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73a7cf7a-ab4f-4090-9ec2-05ed0167e688",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}