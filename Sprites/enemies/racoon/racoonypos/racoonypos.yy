{
    "id": "3eab503c-18c7-4d42-89c4-494d11f424d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "racoonypos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e74e5d19-0d76-4ad0-9026-36acbcabee87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3eab503c-18c7-4d42-89c4-494d11f424d4",
            "compositeImage": {
                "id": "1b70d1ad-e07e-4571-a9cf-36c84fb965e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e74e5d19-0d76-4ad0-9026-36acbcabee87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bce2d1c-13f7-482d-922e-6853caf8e377",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e74e5d19-0d76-4ad0-9026-36acbcabee87",
                    "LayerId": "1103cfba-dbfe-4fb5-a339-045077ced6f7"
                }
            ]
        },
        {
            "id": "b057a999-af86-43a4-81cd-d73fba0bb235",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3eab503c-18c7-4d42-89c4-494d11f424d4",
            "compositeImage": {
                "id": "9b7aedd8-c2ce-4f74-bd71-30131e8a135b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b057a999-af86-43a4-81cd-d73fba0bb235",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fc04e9e-170a-41eb-abf3-c87f2de6d9cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b057a999-af86-43a4-81cd-d73fba0bb235",
                    "LayerId": "1103cfba-dbfe-4fb5-a339-045077ced6f7"
                }
            ]
        },
        {
            "id": "62fe8f16-776d-4415-b687-093892c0dc93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3eab503c-18c7-4d42-89c4-494d11f424d4",
            "compositeImage": {
                "id": "6965e5ef-5e45-4f2d-9df1-b72a9fd5404d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62fe8f16-776d-4415-b687-093892c0dc93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2d5f844-fb93-46f9-9d04-86484dc9c553",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62fe8f16-776d-4415-b687-093892c0dc93",
                    "LayerId": "1103cfba-dbfe-4fb5-a339-045077ced6f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "1103cfba-dbfe-4fb5-a339-045077ced6f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3eab503c-18c7-4d42-89c4-494d11f424d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}