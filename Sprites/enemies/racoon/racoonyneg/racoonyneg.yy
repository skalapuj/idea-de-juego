{
    "id": "f0d0652c-6c63-4a1a-95b3-aacc391fe8b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "racoonyneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "abf4b45f-da05-4d43-ba0c-369a398f8074",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0d0652c-6c63-4a1a-95b3-aacc391fe8b9",
            "compositeImage": {
                "id": "63343872-d2ef-4d03-b15c-efc8e15998de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abf4b45f-da05-4d43-ba0c-369a398f8074",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a12a98e-67bd-434b-a400-bcb4e100a640",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abf4b45f-da05-4d43-ba0c-369a398f8074",
                    "LayerId": "39449566-f87c-4d84-b924-1e786228b37a"
                }
            ]
        },
        {
            "id": "45649a46-b5d0-438e-8ba1-1eb2d00c5af7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0d0652c-6c63-4a1a-95b3-aacc391fe8b9",
            "compositeImage": {
                "id": "9015bb65-fb7c-4fd5-a2f7-256b5ff83a4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45649a46-b5d0-438e-8ba1-1eb2d00c5af7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2008d776-ec06-4ea9-93fe-c579088c0a66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45649a46-b5d0-438e-8ba1-1eb2d00c5af7",
                    "LayerId": "39449566-f87c-4d84-b924-1e786228b37a"
                }
            ]
        },
        {
            "id": "abe11971-cbf3-4fe4-844e-4ef32365c526",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0d0652c-6c63-4a1a-95b3-aacc391fe8b9",
            "compositeImage": {
                "id": "3590d337-ceed-438c-9f3a-79590ef97d59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abe11971-cbf3-4fe4-844e-4ef32365c526",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bafddc0-62f9-44dc-903d-6b33f7bdd40c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abe11971-cbf3-4fe4-844e-4ef32365c526",
                    "LayerId": "39449566-f87c-4d84-b924-1e786228b37a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "39449566-f87c-4d84-b924-1e786228b37a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f0d0652c-6c63-4a1a-95b3-aacc391fe8b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}