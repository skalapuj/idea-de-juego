{
    "id": "4e233122-72fb-43cd-aa26-4d80cbb22521",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "fishypos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "208e5501-612c-42c2-8c91-20d241e1a56c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e233122-72fb-43cd-aa26-4d80cbb22521",
            "compositeImage": {
                "id": "97cc28bb-b58e-41a0-967f-ad03930fa544",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "208e5501-612c-42c2-8c91-20d241e1a56c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea7bdba6-05a6-44a2-b4b3-f9ecbfbdf774",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "208e5501-612c-42c2-8c91-20d241e1a56c",
                    "LayerId": "4286bf16-08fd-421d-86c9-c1dd69a92919"
                }
            ]
        },
        {
            "id": "938d686b-66a9-4092-b612-27285c92c1c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e233122-72fb-43cd-aa26-4d80cbb22521",
            "compositeImage": {
                "id": "6c8c832e-f0de-4b07-a64b-7d67a6324906",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "938d686b-66a9-4092-b612-27285c92c1c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04404eac-1318-4fe9-957c-e28c0a06f87b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "938d686b-66a9-4092-b612-27285c92c1c7",
                    "LayerId": "4286bf16-08fd-421d-86c9-c1dd69a92919"
                }
            ]
        },
        {
            "id": "93bf39eb-7b52-45cd-b6df-e8c3d4692901",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e233122-72fb-43cd-aa26-4d80cbb22521",
            "compositeImage": {
                "id": "72052391-a0cf-4c79-8690-b21d773d3ba3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93bf39eb-7b52-45cd-b6df-e8c3d4692901",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8236938-07e5-41ca-bf47-5b533b1702f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93bf39eb-7b52-45cd-b6df-e8c3d4692901",
                    "LayerId": "4286bf16-08fd-421d-86c9-c1dd69a92919"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "4286bf16-08fd-421d-86c9-c1dd69a92919",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e233122-72fb-43cd-aa26-4d80cbb22521",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}