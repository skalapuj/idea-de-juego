{
    "id": "bf61f23f-a8b2-4e92-aad0-cf34cd4516c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "fishxpos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 4,
    "bbox_right": 25,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5dd62a47-23ae-48ad-87bc-a4ded96bff4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf61f23f-a8b2-4e92-aad0-cf34cd4516c8",
            "compositeImage": {
                "id": "d74c4cbc-0ac2-4c12-bf6e-526a1c40c924",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dd62a47-23ae-48ad-87bc-a4ded96bff4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04a42e6d-65cd-48d9-aa85-72d5490fbfdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dd62a47-23ae-48ad-87bc-a4ded96bff4a",
                    "LayerId": "bc8086bf-9db7-4bdd-8ed1-8d091897c6dc"
                }
            ]
        },
        {
            "id": "68a03d4d-a25a-4af7-b55a-0270d49cd1f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf61f23f-a8b2-4e92-aad0-cf34cd4516c8",
            "compositeImage": {
                "id": "d1eabe46-ef9c-4cba-8634-707f82adce46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68a03d4d-a25a-4af7-b55a-0270d49cd1f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "815a8e8a-2091-4466-a081-2050cbec69cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68a03d4d-a25a-4af7-b55a-0270d49cd1f1",
                    "LayerId": "bc8086bf-9db7-4bdd-8ed1-8d091897c6dc"
                }
            ]
        },
        {
            "id": "59109521-26cc-48cb-bc1d-6deb26163ce6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf61f23f-a8b2-4e92-aad0-cf34cd4516c8",
            "compositeImage": {
                "id": "cdf9cc22-b79c-41f6-afb0-40afdae1e800",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59109521-26cc-48cb-bc1d-6deb26163ce6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0106f99-dc93-4b78-8843-c6532445554d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59109521-26cc-48cb-bc1d-6deb26163ce6",
                    "LayerId": "bc8086bf-9db7-4bdd-8ed1-8d091897c6dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "bc8086bf-9db7-4bdd-8ed1-8d091897c6dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf61f23f-a8b2-4e92-aad0-cf34cd4516c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}