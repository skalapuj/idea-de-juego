{
    "id": "0b7670e3-a49a-4182-9e0c-043d16a119f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "fishxneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 6,
    "bbox_right": 26,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7898bb03-ecd9-4f5b-96b7-41b902d2c855",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b7670e3-a49a-4182-9e0c-043d16a119f5",
            "compositeImage": {
                "id": "0c68de15-a074-41da-bc6f-948fbaa331d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7898bb03-ecd9-4f5b-96b7-41b902d2c855",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d1b134a-537b-47a1-a415-aa7da3de7ab3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7898bb03-ecd9-4f5b-96b7-41b902d2c855",
                    "LayerId": "502c0514-278a-43a5-ab80-15654f8f07ec"
                }
            ]
        },
        {
            "id": "f76eeae4-65b4-4425-a3da-70f817cc0d59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b7670e3-a49a-4182-9e0c-043d16a119f5",
            "compositeImage": {
                "id": "d76bf2e6-dfb5-41ce-9bff-3d927599b710",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f76eeae4-65b4-4425-a3da-70f817cc0d59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1f6c289-a96f-45a6-a288-304a180c8b77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f76eeae4-65b4-4425-a3da-70f817cc0d59",
                    "LayerId": "502c0514-278a-43a5-ab80-15654f8f07ec"
                }
            ]
        },
        {
            "id": "0b6f9119-d75c-4aae-93c4-b9eebeae6963",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b7670e3-a49a-4182-9e0c-043d16a119f5",
            "compositeImage": {
                "id": "1f9d6d9f-95ca-43b0-a4f7-d8559a82dd1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b6f9119-d75c-4aae-93c4-b9eebeae6963",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f31fab8d-b7dd-49aa-8452-f801a0b406e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b6f9119-d75c-4aae-93c4-b9eebeae6963",
                    "LayerId": "502c0514-278a-43a5-ab80-15654f8f07ec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "502c0514-278a-43a5-ab80-15654f8f07ec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b7670e3-a49a-4182-9e0c-043d16a119f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}