{
    "id": "c2b88175-2dfd-44e9-9c5f-031888695163",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "fishyneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89d1e1dd-636f-41f9-a5da-c4bbc94d3719",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b88175-2dfd-44e9-9c5f-031888695163",
            "compositeImage": {
                "id": "e97b7ffc-ef87-4bd0-b737-abbea2e9b06e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89d1e1dd-636f-41f9-a5da-c4bbc94d3719",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "699f2018-81c1-4a0b-8a03-554ea8705b5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89d1e1dd-636f-41f9-a5da-c4bbc94d3719",
                    "LayerId": "f3e9fd84-2f91-4183-b47b-e4922bf8d9f1"
                }
            ]
        },
        {
            "id": "f8be47f0-604a-4998-9c43-585ed49a6462",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b88175-2dfd-44e9-9c5f-031888695163",
            "compositeImage": {
                "id": "1bee661f-c7f2-485a-9844-9d23d99af688",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8be47f0-604a-4998-9c43-585ed49a6462",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13507618-3533-413e-93f1-19ff14ec9eb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8be47f0-604a-4998-9c43-585ed49a6462",
                    "LayerId": "f3e9fd84-2f91-4183-b47b-e4922bf8d9f1"
                }
            ]
        },
        {
            "id": "8d53a5bf-988e-4882-8a8a-a33ac30aedc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2b88175-2dfd-44e9-9c5f-031888695163",
            "compositeImage": {
                "id": "45ac2970-9e6a-4ae9-b3c1-da2b490f1f25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d53a5bf-988e-4882-8a8a-a33ac30aedc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9995543-2363-4cf0-a938-0a77b81d0d82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d53a5bf-988e-4882-8a8a-a33ac30aedc2",
                    "LayerId": "f3e9fd84-2f91-4183-b47b-e4922bf8d9f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "f3e9fd84-2f91-4183-b47b-e4922bf8d9f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2b88175-2dfd-44e9-9c5f-031888695163",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}