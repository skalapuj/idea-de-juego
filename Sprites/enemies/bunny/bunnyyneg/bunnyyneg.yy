{
    "id": "38b5c62f-de3f-4002-b803-7048a9f7e63d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bunnyyneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 13,
    "bbox_right": 31,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5931bea-c93e-4da2-a50b-11b97dfc2798",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38b5c62f-de3f-4002-b803-7048a9f7e63d",
            "compositeImage": {
                "id": "5aa2a619-b7ab-4cb4-aa7c-02fa087cd773",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5931bea-c93e-4da2-a50b-11b97dfc2798",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93332da3-524b-47ba-9765-00b07bc24582",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5931bea-c93e-4da2-a50b-11b97dfc2798",
                    "LayerId": "b0592360-9a1d-444c-afe7-50b913b833b1"
                }
            ]
        },
        {
            "id": "2f902f4f-9ea3-4093-9b78-8152f8f96e95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38b5c62f-de3f-4002-b803-7048a9f7e63d",
            "compositeImage": {
                "id": "12a1dba4-fbf0-4d69-9af7-978f7433d239",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f902f4f-9ea3-4093-9b78-8152f8f96e95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0630fc5f-d672-4598-8953-9b83d3d7bde7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f902f4f-9ea3-4093-9b78-8152f8f96e95",
                    "LayerId": "b0592360-9a1d-444c-afe7-50b913b833b1"
                }
            ]
        },
        {
            "id": "860afec2-4df7-465d-b31e-0cb49bb7312b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38b5c62f-de3f-4002-b803-7048a9f7e63d",
            "compositeImage": {
                "id": "dde6dc86-2580-4429-8898-6a2e49235113",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "860afec2-4df7-465d-b31e-0cb49bb7312b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "497577a4-12ea-4226-bb10-ac4915e726a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "860afec2-4df7-465d-b31e-0cb49bb7312b",
                    "LayerId": "b0592360-9a1d-444c-afe7-50b913b833b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "b0592360-9a1d-444c-afe7-50b913b833b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38b5c62f-de3f-4002-b803-7048a9f7e63d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}