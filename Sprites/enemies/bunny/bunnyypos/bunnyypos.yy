{
    "id": "a2c0f815-d194-4cab-b563-643a26497774",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bunnyypos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 12,
    "bbox_right": 30,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eafcd5c9-6cfc-4212-a90d-2e90d34fa5e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2c0f815-d194-4cab-b563-643a26497774",
            "compositeImage": {
                "id": "f73bf51d-4cbc-4be4-b0f8-9ee84c365b24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eafcd5c9-6cfc-4212-a90d-2e90d34fa5e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abd9ddcb-b807-4bdc-8de7-30efeb26a877",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eafcd5c9-6cfc-4212-a90d-2e90d34fa5e3",
                    "LayerId": "d7d71721-25a6-4df3-af04-bd79b4822f9b"
                }
            ]
        },
        {
            "id": "a4413f01-46b7-49a7-9639-14605a8e6a30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2c0f815-d194-4cab-b563-643a26497774",
            "compositeImage": {
                "id": "37425c77-4174-4a68-970a-48304a47cdcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4413f01-46b7-49a7-9639-14605a8e6a30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92f26ea2-c7a7-4411-bf35-eaa822a39d02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4413f01-46b7-49a7-9639-14605a8e6a30",
                    "LayerId": "d7d71721-25a6-4df3-af04-bd79b4822f9b"
                }
            ]
        },
        {
            "id": "ba729c5a-03dd-4b63-9ec6-b76413c13ca5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2c0f815-d194-4cab-b563-643a26497774",
            "compositeImage": {
                "id": "1f006326-4b69-4e8b-a015-723723a999aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba729c5a-03dd-4b63-9ec6-b76413c13ca5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b1693a9-6735-42fe-8fdd-92f0f153a1cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba729c5a-03dd-4b63-9ec6-b76413c13ca5",
                    "LayerId": "d7d71721-25a6-4df3-af04-bd79b4822f9b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "d7d71721-25a6-4df3-af04-bd79b4822f9b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a2c0f815-d194-4cab-b563-643a26497774",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 53,
    "yorig": -215
}