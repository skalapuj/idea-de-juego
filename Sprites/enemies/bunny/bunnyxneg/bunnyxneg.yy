{
    "id": "fb2f0a7f-1bdb-482d-822a-71dcf41091b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bunnyxneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 34,
    "bbox_left": 8,
    "bbox_right": 36,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f0bd8e5-9958-4403-a5a9-292ad7fe088d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb2f0a7f-1bdb-482d-822a-71dcf41091b0",
            "compositeImage": {
                "id": "cafe78e9-a45d-4774-824a-32ba5ca154d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f0bd8e5-9958-4403-a5a9-292ad7fe088d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1f75c67-6346-433f-8cbe-55bd14d7a094",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f0bd8e5-9958-4403-a5a9-292ad7fe088d",
                    "LayerId": "cf3ccf68-beac-41e5-98a4-cb0806c28041"
                }
            ]
        },
        {
            "id": "cce449cc-b0d4-471e-8342-5028504f1e28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb2f0a7f-1bdb-482d-822a-71dcf41091b0",
            "compositeImage": {
                "id": "66f56b24-3779-449a-bf7f-dbc802a58b86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cce449cc-b0d4-471e-8342-5028504f1e28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be68743a-d037-4587-84fd-e4c6826856ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cce449cc-b0d4-471e-8342-5028504f1e28",
                    "LayerId": "cf3ccf68-beac-41e5-98a4-cb0806c28041"
                }
            ]
        },
        {
            "id": "ccf7ef2a-b1e6-41ac-9280-8331a62272f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb2f0a7f-1bdb-482d-822a-71dcf41091b0",
            "compositeImage": {
                "id": "e46f585f-1fc2-4acb-84be-82b8f6e751ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccf7ef2a-b1e6-41ac-9280-8331a62272f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10911c14-3a29-494f-9ee5-6678b9d490b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccf7ef2a-b1e6-41ac-9280-8331a62272f7",
                    "LayerId": "cf3ccf68-beac-41e5-98a4-cb0806c28041"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "cf3ccf68-beac-41e5-98a4-cb0806c28041",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb2f0a7f-1bdb-482d-822a-71dcf41091b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 16,
    "yorig": 22
}