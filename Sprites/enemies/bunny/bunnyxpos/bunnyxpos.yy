{
    "id": "a33387cf-bb2e-443d-bf4d-0f4de1a9d6fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bunnyxpos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 8,
    "bbox_right": 36,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "44e5af7c-8be2-4041-bad4-f05255bfbcc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a33387cf-bb2e-443d-bf4d-0f4de1a9d6fd",
            "compositeImage": {
                "id": "62a11c11-5f53-4a96-81b4-6414a3582e34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44e5af7c-8be2-4041-bad4-f05255bfbcc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfe18aff-6cc1-478e-b509-bc4f3489a47e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44e5af7c-8be2-4041-bad4-f05255bfbcc5",
                    "LayerId": "f930ddee-33b9-45b1-b412-7fac259f7d38"
                }
            ]
        },
        {
            "id": "8613cf3d-f71e-4efb-b280-42dfc39f5b19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a33387cf-bb2e-443d-bf4d-0f4de1a9d6fd",
            "compositeImage": {
                "id": "35a23e47-7b44-4041-8416-c2c278c5cc05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8613cf3d-f71e-4efb-b280-42dfc39f5b19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbe9f83b-aadd-4f7e-80fe-e46ef67bbbc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8613cf3d-f71e-4efb-b280-42dfc39f5b19",
                    "LayerId": "f930ddee-33b9-45b1-b412-7fac259f7d38"
                }
            ]
        },
        {
            "id": "9b13fde4-3a59-45d9-9d8d-da1a7693cccd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a33387cf-bb2e-443d-bf4d-0f4de1a9d6fd",
            "compositeImage": {
                "id": "fe896fec-3cd2-42d8-a08f-b51797e6cf44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b13fde4-3a59-45d9-9d8d-da1a7693cccd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ae6102c-b2f8-4cd9-99e9-93a34b57a384",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b13fde4-3a59-45d9-9d8d-da1a7693cccd",
                    "LayerId": "f930ddee-33b9-45b1-b412-7fac259f7d38"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "f930ddee-33b9-45b1-b412-7fac259f7d38",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a33387cf-bb2e-443d-bf4d-0f4de1a9d6fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}