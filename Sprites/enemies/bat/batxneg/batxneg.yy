{
    "id": "4797caed-ac97-411a-af93-858abdabd07c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "batxneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 4,
    "bbox_right": 26,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "126a38e6-58a0-4fa7-a522-8c51d34a9185",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4797caed-ac97-411a-af93-858abdabd07c",
            "compositeImage": {
                "id": "fa103f66-231d-4cce-be73-ad9418cfd9e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "126a38e6-58a0-4fa7-a522-8c51d34a9185",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a835942a-8ef6-4852-9943-fbaa8b5dd244",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "126a38e6-58a0-4fa7-a522-8c51d34a9185",
                    "LayerId": "39e3465b-b361-4ecc-b9b4-a871b220f0cc"
                }
            ]
        },
        {
            "id": "ebfc6501-adae-43fb-8e22-5010f9d4a9ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4797caed-ac97-411a-af93-858abdabd07c",
            "compositeImage": {
                "id": "2a519b88-d5c7-4be0-b665-5206afba6528",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebfc6501-adae-43fb-8e22-5010f9d4a9ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29398320-898f-4fa5-8319-536874a41f39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebfc6501-adae-43fb-8e22-5010f9d4a9ef",
                    "LayerId": "39e3465b-b361-4ecc-b9b4-a871b220f0cc"
                }
            ]
        },
        {
            "id": "27552b6c-b139-45fb-aa41-8a07c13062ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4797caed-ac97-411a-af93-858abdabd07c",
            "compositeImage": {
                "id": "d2168db6-5e44-4243-90a6-780ed25cf1bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27552b6c-b139-45fb-aa41-8a07c13062ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bfd1011-8cea-43c2-b057-3b5f06b6fc29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27552b6c-b139-45fb-aa41-8a07c13062ac",
                    "LayerId": "39e3465b-b361-4ecc-b9b4-a871b220f0cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "39e3465b-b361-4ecc-b9b4-a871b220f0cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4797caed-ac97-411a-af93-858abdabd07c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}