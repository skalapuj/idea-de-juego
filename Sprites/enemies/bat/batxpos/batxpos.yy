{
    "id": "5d71b0cd-31fb-4af2-8dd2-fbf260a5bc46",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "batxpos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "329e5083-2191-43fd-8a20-1da5132cf8bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d71b0cd-31fb-4af2-8dd2-fbf260a5bc46",
            "compositeImage": {
                "id": "c65dc877-4c9a-43d8-9f01-b59435aa449a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "329e5083-2191-43fd-8a20-1da5132cf8bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc8213df-1d61-4758-a8b9-b6223e247d7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "329e5083-2191-43fd-8a20-1da5132cf8bf",
                    "LayerId": "0c172ba5-53fd-4fb9-be04-1fe863266345"
                }
            ]
        },
        {
            "id": "8f635a2f-ff60-4b12-8493-8fded66b1460",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d71b0cd-31fb-4af2-8dd2-fbf260a5bc46",
            "compositeImage": {
                "id": "3a00d3c4-38d6-4a1e-8df6-d9175e0b17b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f635a2f-ff60-4b12-8493-8fded66b1460",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "832df18f-1a42-41ee-800a-d0c484cb1e1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f635a2f-ff60-4b12-8493-8fded66b1460",
                    "LayerId": "0c172ba5-53fd-4fb9-be04-1fe863266345"
                }
            ]
        },
        {
            "id": "0d56bd75-5104-4ddf-bb18-7ef1fdc4990d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d71b0cd-31fb-4af2-8dd2-fbf260a5bc46",
            "compositeImage": {
                "id": "5cacbc20-c2c6-4e19-a31a-e5db8197e924",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d56bd75-5104-4ddf-bb18-7ef1fdc4990d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e349849-b929-496c-a237-55916839c744",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d56bd75-5104-4ddf-bb18-7ef1fdc4990d",
                    "LayerId": "0c172ba5-53fd-4fb9-be04-1fe863266345"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0c172ba5-53fd-4fb9-be04-1fe863266345",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d71b0cd-31fb-4af2-8dd2-fbf260a5bc46",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}