{
    "id": "619f2624-7a79-4130-a13e-c7d92b22df1b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "batyneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 1,
    "bbox_right": 32,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6512de17-5f25-462c-90dc-5c03e2ebd0a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "619f2624-7a79-4130-a13e-c7d92b22df1b",
            "compositeImage": {
                "id": "95ba519c-9d9b-4daf-a66b-a3fe492cf364",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6512de17-5f25-462c-90dc-5c03e2ebd0a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b36df1fc-5e2e-4372-9ea7-7eba09245a36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6512de17-5f25-462c-90dc-5c03e2ebd0a7",
                    "LayerId": "0ed77e8b-5766-4ce0-81aa-6272c04881a2"
                }
            ]
        },
        {
            "id": "e86a77bb-a42a-4a34-9093-922e15bd162e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "619f2624-7a79-4130-a13e-c7d92b22df1b",
            "compositeImage": {
                "id": "b1ebf755-f7af-4715-836b-6e8df8946653",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e86a77bb-a42a-4a34-9093-922e15bd162e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4641c28-c289-4f32-8653-c90167209d76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e86a77bb-a42a-4a34-9093-922e15bd162e",
                    "LayerId": "0ed77e8b-5766-4ce0-81aa-6272c04881a2"
                }
            ]
        },
        {
            "id": "1e30fb36-8ca9-43ce-900d-0123b8d2f6e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "619f2624-7a79-4130-a13e-c7d92b22df1b",
            "compositeImage": {
                "id": "d69f67f8-f296-4494-aacc-aa81f7090e2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e30fb36-8ca9-43ce-900d-0123b8d2f6e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ea2b129-86c4-471c-8af7-27d624bb46e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e30fb36-8ca9-43ce-900d-0123b8d2f6e9",
                    "LayerId": "0ed77e8b-5766-4ce0-81aa-6272c04881a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "0ed77e8b-5766-4ce0-81aa-6272c04881a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "619f2624-7a79-4130-a13e-c7d92b22df1b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 34,
    "xorig": 0,
    "yorig": 0
}