{
    "id": "04d06f84-1b1a-45bc-86ab-062ea1c86e20",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "batypos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 32,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "36b8fca9-6601-461a-81a7-014e59505ae3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04d06f84-1b1a-45bc-86ab-062ea1c86e20",
            "compositeImage": {
                "id": "b10deced-26f4-4210-a9e1-af8922e1231b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36b8fca9-6601-461a-81a7-014e59505ae3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fea2be8-2e55-4098-84f6-cf6abcd7f3c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36b8fca9-6601-461a-81a7-014e59505ae3",
                    "LayerId": "8bd22e0a-0a7c-4485-bf96-bb81717f7b4d"
                }
            ]
        },
        {
            "id": "a77f3bb8-331c-4398-b830-4ba87fa48900",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04d06f84-1b1a-45bc-86ab-062ea1c86e20",
            "compositeImage": {
                "id": "4259333f-2b73-4d20-b105-d3a8bd992311",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a77f3bb8-331c-4398-b830-4ba87fa48900",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "670f6746-f0c6-4267-8865-f98b3a7d83f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a77f3bb8-331c-4398-b830-4ba87fa48900",
                    "LayerId": "8bd22e0a-0a7c-4485-bf96-bb81717f7b4d"
                }
            ]
        },
        {
            "id": "8172d2cd-cd5d-40d9-a5a8-0be7b45de067",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04d06f84-1b1a-45bc-86ab-062ea1c86e20",
            "compositeImage": {
                "id": "fea4a239-98bd-46a4-9504-c9cf216610af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8172d2cd-cd5d-40d9-a5a8-0be7b45de067",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5790494a-d85e-4b56-83e9-9c8aab28a85b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8172d2cd-cd5d-40d9-a5a8-0be7b45de067",
                    "LayerId": "8bd22e0a-0a7c-4485-bf96-bb81717f7b4d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8bd22e0a-0a7c-4485-bf96-bb81717f7b4d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "04d06f84-1b1a-45bc-86ab-062ea1c86e20",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 34,
    "xorig": 0,
    "yorig": 0
}