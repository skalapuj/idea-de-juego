{
    "id": "d1d14228-931b-4364-aecb-48b17057e9ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "pandaypos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 10,
    "bbox_right": 37,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bdcad53d-79d3-4bbf-bcf2-59c02ae43c5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1d14228-931b-4364-aecb-48b17057e9ce",
            "compositeImage": {
                "id": "66765e10-c447-4bc1-9c38-87b0d672dee5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdcad53d-79d3-4bbf-bcf2-59c02ae43c5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b476e45d-1747-49e1-ac2a-20a67400ee7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdcad53d-79d3-4bbf-bcf2-59c02ae43c5e",
                    "LayerId": "6b0160fd-12ab-43de-b9b7-390d8aad9a44"
                }
            ]
        },
        {
            "id": "12b46552-ed44-4254-9a26-5332e9d52667",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1d14228-931b-4364-aecb-48b17057e9ce",
            "compositeImage": {
                "id": "34dd6fed-d8c9-4f79-83d1-391a349e0633",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12b46552-ed44-4254-9a26-5332e9d52667",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddb378a2-53ff-4e57-b2c2-99e31224af45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12b46552-ed44-4254-9a26-5332e9d52667",
                    "LayerId": "6b0160fd-12ab-43de-b9b7-390d8aad9a44"
                }
            ]
        },
        {
            "id": "199a7876-bc74-4ab3-8a4a-ffdb65ad9d8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1d14228-931b-4364-aecb-48b17057e9ce",
            "compositeImage": {
                "id": "bf3fa976-20e6-4277-ae7c-9f323d463989",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "199a7876-bc74-4ab3-8a4a-ffdb65ad9d8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a72b394e-a01d-4bae-b39a-185ee582d2be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "199a7876-bc74-4ab3-8a4a-ffdb65ad9d8c",
                    "LayerId": "6b0160fd-12ab-43de-b9b7-390d8aad9a44"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "6b0160fd-12ab-43de-b9b7-390d8aad9a44",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1d14228-931b-4364-aecb-48b17057e9ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 12,
    "yorig": 28
}