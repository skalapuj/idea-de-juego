{
    "id": "57b16fcf-db71-4a9c-9b11-1ec579467951",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "pandaxneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 0,
    "bbox_right": 46,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bc23b5f3-aa42-41e3-8d8b-4cfff8689b14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57b16fcf-db71-4a9c-9b11-1ec579467951",
            "compositeImage": {
                "id": "a5faad12-8ca6-49dc-87b1-b5b2d34e82b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc23b5f3-aa42-41e3-8d8b-4cfff8689b14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36bb926f-e1b2-42aa-9d0a-c7d4679f6d2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc23b5f3-aa42-41e3-8d8b-4cfff8689b14",
                    "LayerId": "9d3f7652-f741-40c5-998d-004202ee649c"
                }
            ]
        },
        {
            "id": "676ef570-0bf0-4480-8f66-de599cecdda2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57b16fcf-db71-4a9c-9b11-1ec579467951",
            "compositeImage": {
                "id": "a00c0cc1-1868-41b6-8270-1404f240803c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "676ef570-0bf0-4480-8f66-de599cecdda2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "642ff8a7-0a68-45da-acad-c6beb2c092ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "676ef570-0bf0-4480-8f66-de599cecdda2",
                    "LayerId": "9d3f7652-f741-40c5-998d-004202ee649c"
                }
            ]
        },
        {
            "id": "7f27866e-ee24-4521-a6b5-4c3cc15db54c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57b16fcf-db71-4a9c-9b11-1ec579467951",
            "compositeImage": {
                "id": "501b1099-3ddd-4e2e-ae2a-28a767fc254d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f27866e-ee24-4521-a6b5-4c3cc15db54c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "443d2469-447b-4b44-a159-0d56b9a5e764",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f27866e-ee24-4521-a6b5-4c3cc15db54c",
                    "LayerId": "9d3f7652-f741-40c5-998d-004202ee649c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "9d3f7652-f741-40c5-998d-004202ee649c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57b16fcf-db71-4a9c-9b11-1ec579467951",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}