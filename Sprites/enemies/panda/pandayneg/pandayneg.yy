{
    "id": "b450ca6c-ed6e-4d4d-ade6-5693ca1fdfaa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "pandayneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 11,
    "bbox_right": 37,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae5c0ed5-95be-4dab-92ac-b44dc81e7c1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b450ca6c-ed6e-4d4d-ade6-5693ca1fdfaa",
            "compositeImage": {
                "id": "348b8d07-9c1f-4783-869b-fdeffc027a0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae5c0ed5-95be-4dab-92ac-b44dc81e7c1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50fcb82b-fb77-470a-a599-92b61286543c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae5c0ed5-95be-4dab-92ac-b44dc81e7c1f",
                    "LayerId": "ab18a93f-eae4-4bdd-83e6-ab2284af6ae2"
                }
            ]
        },
        {
            "id": "417e0fed-6455-4bef-ae33-8f21e53fa772",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b450ca6c-ed6e-4d4d-ade6-5693ca1fdfaa",
            "compositeImage": {
                "id": "74027234-3ee1-4c4e-bd61-fada5f224c22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "417e0fed-6455-4bef-ae33-8f21e53fa772",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87fe0b5c-4323-4c0d-b9ec-e4adcd91e4d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "417e0fed-6455-4bef-ae33-8f21e53fa772",
                    "LayerId": "ab18a93f-eae4-4bdd-83e6-ab2284af6ae2"
                }
            ]
        },
        {
            "id": "38cf2cec-4a5e-4f03-a90d-cbb1e5404fc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b450ca6c-ed6e-4d4d-ade6-5693ca1fdfaa",
            "compositeImage": {
                "id": "d0a38647-7639-45a6-acce-e585e0d5d9b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38cf2cec-4a5e-4f03-a90d-cbb1e5404fc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bb3b551-4ece-4913-a2d6-d87a90871979",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38cf2cec-4a5e-4f03-a90d-cbb1e5404fc1",
                    "LayerId": "ab18a93f-eae4-4bdd-83e6-ab2284af6ae2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "ab18a93f-eae4-4bdd-83e6-ab2284af6ae2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b450ca6c-ed6e-4d4d-ade6-5693ca1fdfaa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}