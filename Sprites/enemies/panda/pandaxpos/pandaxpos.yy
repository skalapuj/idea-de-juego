{
    "id": "10ad30de-61f9-497a-a4f2-7c77b19297fb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "pandaxpos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 0,
    "bbox_right": 46,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "14281a5f-1197-4973-9785-2717f6df3c42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10ad30de-61f9-497a-a4f2-7c77b19297fb",
            "compositeImage": {
                "id": "2cb69656-59ca-4b08-ba58-2c6518dae557",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14281a5f-1197-4973-9785-2717f6df3c42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46b1db47-a72f-4d49-845b-b7ea19a6ee78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14281a5f-1197-4973-9785-2717f6df3c42",
                    "LayerId": "847cceae-8783-48e6-89d4-1402704f7a46"
                }
            ]
        },
        {
            "id": "ceee969d-bb66-47c4-8ebb-3a15543711d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10ad30de-61f9-497a-a4f2-7c77b19297fb",
            "compositeImage": {
                "id": "dd8501c8-a072-4a1b-877d-36d4dacc08f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ceee969d-bb66-47c4-8ebb-3a15543711d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c04324e4-98a1-4c0c-adda-3d6bd726eef7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ceee969d-bb66-47c4-8ebb-3a15543711d5",
                    "LayerId": "847cceae-8783-48e6-89d4-1402704f7a46"
                }
            ]
        },
        {
            "id": "a985c917-8471-4095-999e-2b30919f9cea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10ad30de-61f9-497a-a4f2-7c77b19297fb",
            "compositeImage": {
                "id": "1feca5d7-727b-4b90-be87-f8ed45bcfb00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a985c917-8471-4095-999e-2b30919f9cea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4daf2b13-88d7-4915-96ba-82daec1fc86e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a985c917-8471-4095-999e-2b30919f9cea",
                    "LayerId": "847cceae-8783-48e6-89d4-1402704f7a46"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "847cceae-8783-48e6-89d4-1402704f7a46",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10ad30de-61f9-497a-a4f2-7c77b19297fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}