{
    "id": "c29fecd6-c69a-4646-8c3b-e5dbe6d70b7d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rat5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 13,
    "bbox_right": 37,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "afd7e5b3-6730-4579-b6de-52234ecfe52b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c29fecd6-c69a-4646-8c3b-e5dbe6d70b7d",
            "compositeImage": {
                "id": "351842a2-9e02-4f6d-a841-faec9b6916d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afd7e5b3-6730-4579-b6de-52234ecfe52b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93501fe2-08f1-4f80-a610-de4cb2d0922c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afd7e5b3-6730-4579-b6de-52234ecfe52b",
                    "LayerId": "03678ff8-9916-4fea-bae4-a2a9976de61a"
                }
            ]
        },
        {
            "id": "d2645fe7-942f-4d78-84f5-11c6384f06b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c29fecd6-c69a-4646-8c3b-e5dbe6d70b7d",
            "compositeImage": {
                "id": "16d86679-3959-45c0-b46a-13c4a9555c7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2645fe7-942f-4d78-84f5-11c6384f06b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98df6ee9-28b5-4dec-9b31-bab950d02179",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2645fe7-942f-4d78-84f5-11c6384f06b4",
                    "LayerId": "03678ff8-9916-4fea-bae4-a2a9976de61a"
                }
            ]
        },
        {
            "id": "06d02754-58d9-498a-a9bb-1250aa4c0946",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c29fecd6-c69a-4646-8c3b-e5dbe6d70b7d",
            "compositeImage": {
                "id": "d1910df6-573b-4cb9-b259-638379e2f331",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06d02754-58d9-498a-a9bb-1250aa4c0946",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53b7b8a2-65fe-43a1-aaac-df4de63396f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06d02754-58d9-498a-a9bb-1250aa4c0946",
                    "LayerId": "03678ff8-9916-4fea-bae4-a2a9976de61a"
                }
            ]
        },
        {
            "id": "67a4dec6-fdb6-458c-a550-f0f4ab901b37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c29fecd6-c69a-4646-8c3b-e5dbe6d70b7d",
            "compositeImage": {
                "id": "16a93c87-80d1-446e-9a67-b73d53d91468",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67a4dec6-fdb6-458c-a550-f0f4ab901b37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d4037eb-1bd1-408a-8e61-536cbaccad4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67a4dec6-fdb6-458c-a550-f0f4ab901b37",
                    "LayerId": "03678ff8-9916-4fea-bae4-a2a9976de61a"
                }
            ]
        },
        {
            "id": "dc0a5567-6372-41b8-bb7a-3825726fe1b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c29fecd6-c69a-4646-8c3b-e5dbe6d70b7d",
            "compositeImage": {
                "id": "8a16c3a6-af78-40e1-bd22-a3ae447c8c8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc0a5567-6372-41b8-bb7a-3825726fe1b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aecf865b-8340-4044-8817-bdc89ade38a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc0a5567-6372-41b8-bb7a-3825726fe1b7",
                    "LayerId": "03678ff8-9916-4fea-bae4-a2a9976de61a"
                }
            ]
        },
        {
            "id": "470b4ae7-f4cc-4f8c-a8ea-74120ef10d5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c29fecd6-c69a-4646-8c3b-e5dbe6d70b7d",
            "compositeImage": {
                "id": "77de17be-cabc-467c-a773-04dedea317de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "470b4ae7-f4cc-4f8c-a8ea-74120ef10d5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a5fbd7f-889f-4b4f-bff6-6e1d95424dcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "470b4ae7-f4cc-4f8c-a8ea-74120ef10d5e",
                    "LayerId": "03678ff8-9916-4fea-bae4-a2a9976de61a"
                }
            ]
        },
        {
            "id": "08945eef-4081-4c17-abf3-e06573d04adf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c29fecd6-c69a-4646-8c3b-e5dbe6d70b7d",
            "compositeImage": {
                "id": "c8b2a900-6737-4c0f-9548-80bd0a833fd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08945eef-4081-4c17-abf3-e06573d04adf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad6422a9-15a4-4e59-bd3e-6863a18afb17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08945eef-4081-4c17-abf3-e06573d04adf",
                    "LayerId": "03678ff8-9916-4fea-bae4-a2a9976de61a"
                }
            ]
        },
        {
            "id": "b77dc942-1596-4010-887a-41fd4ef51e6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c29fecd6-c69a-4646-8c3b-e5dbe6d70b7d",
            "compositeImage": {
                "id": "e2c0d6c2-dbeb-45ac-a92a-2cbbe684f986",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b77dc942-1596-4010-887a-41fd4ef51e6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2f7242b-f88d-4323-97f5-0a47c6c42daf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b77dc942-1596-4010-887a-41fd4ef51e6f",
                    "LayerId": "03678ff8-9916-4fea-bae4-a2a9976de61a"
                }
            ]
        },
        {
            "id": "c6b9a7db-fe76-4262-b69c-bc6a2723624c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c29fecd6-c69a-4646-8c3b-e5dbe6d70b7d",
            "compositeImage": {
                "id": "edf6d5f3-37df-4d52-be73-8983bc7f838d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6b9a7db-fe76-4262-b69c-bc6a2723624c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43e24b9d-297f-4e16-b5a7-953d06ea508f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6b9a7db-fe76-4262-b69c-bc6a2723624c",
                    "LayerId": "03678ff8-9916-4fea-bae4-a2a9976de61a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 47,
    "layers": [
        {
            "id": "03678ff8-9916-4fea-bae4-a2a9976de61a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c29fecd6-c69a-4646-8c3b-e5dbe6d70b7d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}