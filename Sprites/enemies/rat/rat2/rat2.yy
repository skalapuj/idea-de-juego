{
    "id": "86b939ac-a196-44ac-a226-1ed2c0a958eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rat2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 44,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cf729e43-4891-435e-8c1e-06c5db6f21f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86b939ac-a196-44ac-a226-1ed2c0a958eb",
            "compositeImage": {
                "id": "9e746761-979b-4dce-9b24-9be84e102303",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf729e43-4891-435e-8c1e-06c5db6f21f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "720cffe3-177c-438a-9b75-b2f289baed5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf729e43-4891-435e-8c1e-06c5db6f21f0",
                    "LayerId": "d5f937e1-2288-4c94-8b38-03749954290b"
                }
            ]
        },
        {
            "id": "80b8c680-25aa-427d-af50-0d2b98288b7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86b939ac-a196-44ac-a226-1ed2c0a958eb",
            "compositeImage": {
                "id": "35ff6808-f840-4d3d-b72e-1423badb663c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80b8c680-25aa-427d-af50-0d2b98288b7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "019349e5-5a15-49fb-b24d-5085cac3821f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80b8c680-25aa-427d-af50-0d2b98288b7d",
                    "LayerId": "d5f937e1-2288-4c94-8b38-03749954290b"
                }
            ]
        },
        {
            "id": "7ee0e9e2-be5d-4fb9-922a-a74a9267719e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86b939ac-a196-44ac-a226-1ed2c0a958eb",
            "compositeImage": {
                "id": "637e9d3e-4e32-4527-945b-a0fa51b64c24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ee0e9e2-be5d-4fb9-922a-a74a9267719e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dc4cfef-e567-48af-af9d-9237e5334dd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ee0e9e2-be5d-4fb9-922a-a74a9267719e",
                    "LayerId": "d5f937e1-2288-4c94-8b38-03749954290b"
                }
            ]
        },
        {
            "id": "6dcc90e9-8f35-4d04-a6c7-2c82c97edaa9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86b939ac-a196-44ac-a226-1ed2c0a958eb",
            "compositeImage": {
                "id": "92d77ecb-bb7d-496f-a289-cf68a7dd0d82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dcc90e9-8f35-4d04-a6c7-2c82c97edaa9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81624bf9-eeff-410a-b2ac-9fe44b5b3d95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dcc90e9-8f35-4d04-a6c7-2c82c97edaa9",
                    "LayerId": "d5f937e1-2288-4c94-8b38-03749954290b"
                }
            ]
        },
        {
            "id": "d9cff5b5-1315-4b75-ad89-0bf7774274a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86b939ac-a196-44ac-a226-1ed2c0a958eb",
            "compositeImage": {
                "id": "89dd641c-c824-4347-aa2f-75a007de6718",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9cff5b5-1315-4b75-ad89-0bf7774274a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a499bc8-a9b8-407a-9969-c6224752eb52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9cff5b5-1315-4b75-ad89-0bf7774274a8",
                    "LayerId": "d5f937e1-2288-4c94-8b38-03749954290b"
                }
            ]
        },
        {
            "id": "f9918742-b2c5-47c2-95a0-23c7141e4fb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86b939ac-a196-44ac-a226-1ed2c0a958eb",
            "compositeImage": {
                "id": "66138108-2cf4-4e9d-9eaf-4993798fe188",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9918742-b2c5-47c2-95a0-23c7141e4fb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5c31b48-b9c8-4564-aac1-94dbc9ad3f54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9918742-b2c5-47c2-95a0-23c7141e4fb0",
                    "LayerId": "d5f937e1-2288-4c94-8b38-03749954290b"
                }
            ]
        },
        {
            "id": "6ff95d05-2468-4b5a-9f09-a07cf71cc65a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86b939ac-a196-44ac-a226-1ed2c0a958eb",
            "compositeImage": {
                "id": "57710f55-e9fa-4b3e-91f7-fe6335a40107",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ff95d05-2468-4b5a-9f09-a07cf71cc65a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4dc63fe-2ec1-40ef-9b6e-3db76a53a1b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ff95d05-2468-4b5a-9f09-a07cf71cc65a",
                    "LayerId": "d5f937e1-2288-4c94-8b38-03749954290b"
                }
            ]
        },
        {
            "id": "d57a7f5c-b6c0-4835-903e-d68171b1520f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86b939ac-a196-44ac-a226-1ed2c0a958eb",
            "compositeImage": {
                "id": "e62a8e30-6bd8-4fd0-b77b-6760500617b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d57a7f5c-b6c0-4835-903e-d68171b1520f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9424ca3f-99a5-4196-9392-cbbeb390f6bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d57a7f5c-b6c0-4835-903e-d68171b1520f",
                    "LayerId": "d5f937e1-2288-4c94-8b38-03749954290b"
                }
            ]
        },
        {
            "id": "74bb5b16-aeb9-4c45-9ba1-7c68235ff993",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86b939ac-a196-44ac-a226-1ed2c0a958eb",
            "compositeImage": {
                "id": "db569708-3178-4ad8-a049-3a015a955208",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74bb5b16-aeb9-4c45-9ba1-7c68235ff993",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9eb72a4-25ed-4398-98c0-305c0009511b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74bb5b16-aeb9-4c45-9ba1-7c68235ff993",
                    "LayerId": "d5f937e1-2288-4c94-8b38-03749954290b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 47,
    "layers": [
        {
            "id": "d5f937e1-2288-4c94-8b38-03749954290b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "86b939ac-a196-44ac-a226-1ed2c0a958eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}