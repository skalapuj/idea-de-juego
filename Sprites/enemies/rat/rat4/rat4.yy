{
    "id": "09d68a86-d237-492e-a33e-951a1d1ec65a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rat4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 14,
    "bbox_right": 40,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d5424f0-eeea-4b20-9865-598c32e55cff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09d68a86-d237-492e-a33e-951a1d1ec65a",
            "compositeImage": {
                "id": "6eaed369-62a8-432e-899e-94706955bcea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d5424f0-eeea-4b20-9865-598c32e55cff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77f5cd1c-2841-4cfb-9913-cdf68b379dd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d5424f0-eeea-4b20-9865-598c32e55cff",
                    "LayerId": "3c0018f4-0388-4e41-aa05-21831ece418b"
                }
            ]
        },
        {
            "id": "f9757610-3568-4b14-a006-d972aee8cac8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09d68a86-d237-492e-a33e-951a1d1ec65a",
            "compositeImage": {
                "id": "5799d920-5423-42a3-a948-c813a5fe1d3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9757610-3568-4b14-a006-d972aee8cac8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b77d6412-1609-456d-bf92-c57fcc1a52d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9757610-3568-4b14-a006-d972aee8cac8",
                    "LayerId": "3c0018f4-0388-4e41-aa05-21831ece418b"
                }
            ]
        },
        {
            "id": "90d19158-c55a-4a36-85f1-da286c000057",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09d68a86-d237-492e-a33e-951a1d1ec65a",
            "compositeImage": {
                "id": "58cd0983-079b-41d4-8670-318285673f71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90d19158-c55a-4a36-85f1-da286c000057",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94d9d646-e433-47fd-9de3-763e1e1b39ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90d19158-c55a-4a36-85f1-da286c000057",
                    "LayerId": "3c0018f4-0388-4e41-aa05-21831ece418b"
                }
            ]
        },
        {
            "id": "b88c300f-6d4a-41ed-aab4-22426ae59421",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09d68a86-d237-492e-a33e-951a1d1ec65a",
            "compositeImage": {
                "id": "705efb9a-660b-42e8-9d14-72dfc7b62fd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b88c300f-6d4a-41ed-aab4-22426ae59421",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7e1beb2-975e-4200-9097-eea0423b5ef4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b88c300f-6d4a-41ed-aab4-22426ae59421",
                    "LayerId": "3c0018f4-0388-4e41-aa05-21831ece418b"
                }
            ]
        },
        {
            "id": "6edcd54b-6d3a-4bf7-8573-b9eb42e54e11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09d68a86-d237-492e-a33e-951a1d1ec65a",
            "compositeImage": {
                "id": "3468b7f9-9b77-4b22-a91f-a5fc1814a007",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6edcd54b-6d3a-4bf7-8573-b9eb42e54e11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c805257c-d21a-4d04-992e-bee3f8e0cb8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6edcd54b-6d3a-4bf7-8573-b9eb42e54e11",
                    "LayerId": "3c0018f4-0388-4e41-aa05-21831ece418b"
                }
            ]
        },
        {
            "id": "6a4fbc51-62f9-4c4c-930c-f986432bdbb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09d68a86-d237-492e-a33e-951a1d1ec65a",
            "compositeImage": {
                "id": "87df94fb-b641-48b1-beb1-19620d5da5ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a4fbc51-62f9-4c4c-930c-f986432bdbb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3302f488-51e1-4f13-ab61-5604ea890dca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a4fbc51-62f9-4c4c-930c-f986432bdbb6",
                    "LayerId": "3c0018f4-0388-4e41-aa05-21831ece418b"
                }
            ]
        },
        {
            "id": "56147d2a-42a5-4b2b-992d-4773dcb84eb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09d68a86-d237-492e-a33e-951a1d1ec65a",
            "compositeImage": {
                "id": "9f5dbdc4-3ad3-4c3a-84f1-fe16480c97af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56147d2a-42a5-4b2b-992d-4773dcb84eb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "992f4c85-3527-4b11-bf18-82b308bd3dac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56147d2a-42a5-4b2b-992d-4773dcb84eb6",
                    "LayerId": "3c0018f4-0388-4e41-aa05-21831ece418b"
                }
            ]
        },
        {
            "id": "49347e59-3e03-4585-9d4b-514946f078fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09d68a86-d237-492e-a33e-951a1d1ec65a",
            "compositeImage": {
                "id": "e1b26689-a943-46ce-8401-f12313002cde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49347e59-3e03-4585-9d4b-514946f078fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba494d68-cd07-47ba-8f61-352ce30d9d9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49347e59-3e03-4585-9d4b-514946f078fe",
                    "LayerId": "3c0018f4-0388-4e41-aa05-21831ece418b"
                }
            ]
        },
        {
            "id": "9033b68f-64e2-41cd-a40c-cfee26cd8aaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09d68a86-d237-492e-a33e-951a1d1ec65a",
            "compositeImage": {
                "id": "0cad77b7-9ca0-476a-90ca-e2d213fd9862",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9033b68f-64e2-41cd-a40c-cfee26cd8aaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6ff348c-15dd-4192-a25f-95d99186d641",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9033b68f-64e2-41cd-a40c-cfee26cd8aaf",
                    "LayerId": "3c0018f4-0388-4e41-aa05-21831ece418b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 47,
    "layers": [
        {
            "id": "3c0018f4-0388-4e41-aa05-21831ece418b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "09d68a86-d237-492e-a33e-951a1d1ec65a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}