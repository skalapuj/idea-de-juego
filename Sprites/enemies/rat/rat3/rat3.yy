{
    "id": "2c1d0c5d-c052-4aed-9ba2-44866f24c7d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rat3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 14,
    "bbox_right": 40,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bbbf7fc8-8610-4a21-befc-a204987e3359",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c1d0c5d-c052-4aed-9ba2-44866f24c7d9",
            "compositeImage": {
                "id": "40871e1d-8c20-417a-89ed-c85dd486a5d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbbf7fc8-8610-4a21-befc-a204987e3359",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2debd502-b325-428f-94d3-0ffef8564b15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbbf7fc8-8610-4a21-befc-a204987e3359",
                    "LayerId": "36eeb4ee-c568-4627-ae96-594ef2ae0c3c"
                }
            ]
        },
        {
            "id": "212b824b-ec5a-41a3-a77a-a70fff0d5f9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c1d0c5d-c052-4aed-9ba2-44866f24c7d9",
            "compositeImage": {
                "id": "014d7757-bfee-4152-8c7f-cbc2e74c16f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "212b824b-ec5a-41a3-a77a-a70fff0d5f9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8285b432-85c5-4451-ab15-657fd44f7622",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "212b824b-ec5a-41a3-a77a-a70fff0d5f9a",
                    "LayerId": "36eeb4ee-c568-4627-ae96-594ef2ae0c3c"
                }
            ]
        },
        {
            "id": "02af7c98-e53b-4804-a5f9-53e81c3606e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c1d0c5d-c052-4aed-9ba2-44866f24c7d9",
            "compositeImage": {
                "id": "f8ceb655-0d42-4a90-bfce-b6d0943ede92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02af7c98-e53b-4804-a5f9-53e81c3606e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1341b59f-ff6f-46b0-ad26-e1c7ac139342",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02af7c98-e53b-4804-a5f9-53e81c3606e5",
                    "LayerId": "36eeb4ee-c568-4627-ae96-594ef2ae0c3c"
                }
            ]
        },
        {
            "id": "322cae9f-e1e7-438a-a1bc-f0b323e37c60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c1d0c5d-c052-4aed-9ba2-44866f24c7d9",
            "compositeImage": {
                "id": "579c8080-b211-4f92-8f5b-38d18aadde38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "322cae9f-e1e7-438a-a1bc-f0b323e37c60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ed32133-e9da-4ed6-bdcd-f55187e4ba95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "322cae9f-e1e7-438a-a1bc-f0b323e37c60",
                    "LayerId": "36eeb4ee-c568-4627-ae96-594ef2ae0c3c"
                }
            ]
        },
        {
            "id": "7b58bfa9-8daf-4993-9cc9-465ccb31acef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c1d0c5d-c052-4aed-9ba2-44866f24c7d9",
            "compositeImage": {
                "id": "2ea09752-8d49-4646-9a08-83b0717453e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b58bfa9-8daf-4993-9cc9-465ccb31acef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca344511-7159-4ecc-ad3c-a236d2445208",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b58bfa9-8daf-4993-9cc9-465ccb31acef",
                    "LayerId": "36eeb4ee-c568-4627-ae96-594ef2ae0c3c"
                }
            ]
        },
        {
            "id": "a9bc5475-99fd-4e28-847b-adf7084171e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c1d0c5d-c052-4aed-9ba2-44866f24c7d9",
            "compositeImage": {
                "id": "98cab9f2-56e5-4915-86a1-f2d50c7fb922",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9bc5475-99fd-4e28-847b-adf7084171e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d97a200-fd48-4496-986f-8ef28f40b974",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9bc5475-99fd-4e28-847b-adf7084171e6",
                    "LayerId": "36eeb4ee-c568-4627-ae96-594ef2ae0c3c"
                }
            ]
        },
        {
            "id": "dfbe4859-1c99-44a2-81aa-404a20b7301d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c1d0c5d-c052-4aed-9ba2-44866f24c7d9",
            "compositeImage": {
                "id": "687d317a-ec7b-41cb-9190-d51b5df298f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfbe4859-1c99-44a2-81aa-404a20b7301d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "450cd8b6-3451-4fd8-b2e1-3ed46acf3abb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfbe4859-1c99-44a2-81aa-404a20b7301d",
                    "LayerId": "36eeb4ee-c568-4627-ae96-594ef2ae0c3c"
                }
            ]
        },
        {
            "id": "ec7dccc2-65af-4175-a708-ff454e08d47f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c1d0c5d-c052-4aed-9ba2-44866f24c7d9",
            "compositeImage": {
                "id": "d6b2f833-ce0a-4fd7-8ac0-3ea731dedb01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec7dccc2-65af-4175-a708-ff454e08d47f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b864cdd-efdc-477f-b0cb-9654be85c466",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec7dccc2-65af-4175-a708-ff454e08d47f",
                    "LayerId": "36eeb4ee-c568-4627-ae96-594ef2ae0c3c"
                }
            ]
        },
        {
            "id": "4a27267b-67b2-48da-849f-ea50296825fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c1d0c5d-c052-4aed-9ba2-44866f24c7d9",
            "compositeImage": {
                "id": "f203aafb-2c7c-40dc-a6e6-3cacdefd339d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a27267b-67b2-48da-849f-ea50296825fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7d79743-0bd9-407b-adb1-63844c7b9efb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a27267b-67b2-48da-849f-ea50296825fe",
                    "LayerId": "36eeb4ee-c568-4627-ae96-594ef2ae0c3c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 47,
    "layers": [
        {
            "id": "36eeb4ee-c568-4627-ae96-594ef2ae0c3c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c1d0c5d-c052-4aed-9ba2-44866f24c7d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}