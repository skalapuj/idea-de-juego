{
    "id": "e6765956-1976-43f0-9761-89d7c944d4d3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rat1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 34,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c5604199-8e5f-4fd8-93c4-f6384604d118",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6765956-1976-43f0-9761-89d7c944d4d3",
            "compositeImage": {
                "id": "12b804fc-32fa-47a6-a129-73918f6c450b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5604199-8e5f-4fd8-93c4-f6384604d118",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17752b57-d210-4213-90b0-d54365d388b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5604199-8e5f-4fd8-93c4-f6384604d118",
                    "LayerId": "630758ff-ceed-4c52-8080-4f77229e7df7"
                }
            ]
        },
        {
            "id": "c5d9e1c3-7fa4-4fb1-9c0a-81d8ecc0f1cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6765956-1976-43f0-9761-89d7c944d4d3",
            "compositeImage": {
                "id": "67ed4eb9-bb23-4746-ab46-3334bed0167b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5d9e1c3-7fa4-4fb1-9c0a-81d8ecc0f1cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aff7608e-dc95-4edf-86c0-40d01d701958",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5d9e1c3-7fa4-4fb1-9c0a-81d8ecc0f1cd",
                    "LayerId": "630758ff-ceed-4c52-8080-4f77229e7df7"
                }
            ]
        },
        {
            "id": "f52ccd81-c7be-4a6a-af4f-e6ad28696a5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6765956-1976-43f0-9761-89d7c944d4d3",
            "compositeImage": {
                "id": "6bbe3c2b-5869-406b-ae98-a5c05336b3cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f52ccd81-c7be-4a6a-af4f-e6ad28696a5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c943ac11-983e-4b2a-8754-16b236478970",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f52ccd81-c7be-4a6a-af4f-e6ad28696a5f",
                    "LayerId": "630758ff-ceed-4c52-8080-4f77229e7df7"
                }
            ]
        },
        {
            "id": "8b3b32d1-4847-42d9-bae0-b53463a641d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6765956-1976-43f0-9761-89d7c944d4d3",
            "compositeImage": {
                "id": "dbe0d290-aacd-435b-be9a-4c683b2bea61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b3b32d1-4847-42d9-bae0-b53463a641d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab37b888-d001-4e3d-9ca5-5e5f58c2bd70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b3b32d1-4847-42d9-bae0-b53463a641d9",
                    "LayerId": "630758ff-ceed-4c52-8080-4f77229e7df7"
                }
            ]
        },
        {
            "id": "cb7b366a-b7ec-429e-a0b8-3aebcb8c057b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6765956-1976-43f0-9761-89d7c944d4d3",
            "compositeImage": {
                "id": "3edf3f37-8ced-430a-8ea5-fe168b416049",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb7b366a-b7ec-429e-a0b8-3aebcb8c057b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dfdc198-3dde-4be0-b58d-62118b13353b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb7b366a-b7ec-429e-a0b8-3aebcb8c057b",
                    "LayerId": "630758ff-ceed-4c52-8080-4f77229e7df7"
                }
            ]
        },
        {
            "id": "353603b3-8c3b-4698-bfb5-6f4793b1879e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6765956-1976-43f0-9761-89d7c944d4d3",
            "compositeImage": {
                "id": "569a4d8b-1dc1-41a3-bcf3-f91617c79c14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "353603b3-8c3b-4698-bfb5-6f4793b1879e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10cb35eb-eba1-4aa9-8f57-e60736d5fdea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "353603b3-8c3b-4698-bfb5-6f4793b1879e",
                    "LayerId": "630758ff-ceed-4c52-8080-4f77229e7df7"
                }
            ]
        },
        {
            "id": "1cc403d8-32ac-4e39-b2f2-a59695c79040",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6765956-1976-43f0-9761-89d7c944d4d3",
            "compositeImage": {
                "id": "9cb80f57-6176-4093-a0b0-4afe5cb73961",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cc403d8-32ac-4e39-b2f2-a59695c79040",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97a8e564-6c25-4bef-986c-82bdfb7279bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cc403d8-32ac-4e39-b2f2-a59695c79040",
                    "LayerId": "630758ff-ceed-4c52-8080-4f77229e7df7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 47,
    "layers": [
        {
            "id": "630758ff-ceed-4c52-8080-4f77229e7df7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6765956-1976-43f0-9761-89d7c944d4d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 47,
    "xorig": 0,
    "yorig": 0
}