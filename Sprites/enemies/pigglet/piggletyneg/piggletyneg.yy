{
    "id": "8f2043b4-e8de-4e40-bf5f-4b02870f650b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "piggletyneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 8,
    "bbox_right": 23,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ae3fa78-eb42-4802-a32e-8ae0e3199807",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f2043b4-e8de-4e40-bf5f-4b02870f650b",
            "compositeImage": {
                "id": "8895d7da-d5d6-4dde-be36-11a5f5526a6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ae3fa78-eb42-4802-a32e-8ae0e3199807",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b08309f9-a626-4567-87fc-dd8ea8fa23d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ae3fa78-eb42-4802-a32e-8ae0e3199807",
                    "LayerId": "e7d4b6a0-3024-4368-8663-1ccd06853c0f"
                }
            ]
        },
        {
            "id": "972df11a-41c8-4052-b402-3cc12c2dfbb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f2043b4-e8de-4e40-bf5f-4b02870f650b",
            "compositeImage": {
                "id": "1716391e-d2f9-4db5-93d0-362336410abd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "972df11a-41c8-4052-b402-3cc12c2dfbb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5ef5ae5-df56-4c35-a5fc-5e0d73729bb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "972df11a-41c8-4052-b402-3cc12c2dfbb5",
                    "LayerId": "e7d4b6a0-3024-4368-8663-1ccd06853c0f"
                }
            ]
        },
        {
            "id": "e7febc3f-0901-49cb-90c0-970ed863aa12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f2043b4-e8de-4e40-bf5f-4b02870f650b",
            "compositeImage": {
                "id": "972a8a7e-a968-41c3-ad72-8c2956f91b2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7febc3f-0901-49cb-90c0-970ed863aa12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "094aab14-7e1d-47c0-bc0c-f139a9a9c6a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7febc3f-0901-49cb-90c0-970ed863aa12",
                    "LayerId": "e7d4b6a0-3024-4368-8663-1ccd06853c0f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e7d4b6a0-3024-4368-8663-1ccd06853c0f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8f2043b4-e8de-4e40-bf5f-4b02870f650b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}