{
    "id": "58fdf3ae-c7e3-41fb-bc9e-5845e2c1d87c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "piggletxneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 4,
    "bbox_right": 25,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "226dde6b-1aff-4d67-b49c-f52eaf16f016",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58fdf3ae-c7e3-41fb-bc9e-5845e2c1d87c",
            "compositeImage": {
                "id": "ce47b80b-4d1d-494d-8425-b4438dd3932c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "226dde6b-1aff-4d67-b49c-f52eaf16f016",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8250b1dc-7e02-4a69-8405-9db9a035dd0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "226dde6b-1aff-4d67-b49c-f52eaf16f016",
                    "LayerId": "8e569032-93c0-4496-8f46-c7d0620fb475"
                }
            ]
        },
        {
            "id": "e3fecdad-a4b4-4ba1-a17e-b6392f2c1ca4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58fdf3ae-c7e3-41fb-bc9e-5845e2c1d87c",
            "compositeImage": {
                "id": "54f6042c-ae52-4a2b-a3ac-56f1255d1c43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3fecdad-a4b4-4ba1-a17e-b6392f2c1ca4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3b970b7-9ff7-4599-ae86-2b98c0607e4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3fecdad-a4b4-4ba1-a17e-b6392f2c1ca4",
                    "LayerId": "8e569032-93c0-4496-8f46-c7d0620fb475"
                }
            ]
        },
        {
            "id": "ebe689ef-49b5-4686-b698-460d6a1ab65e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58fdf3ae-c7e3-41fb-bc9e-5845e2c1d87c",
            "compositeImage": {
                "id": "4fb03baf-09d6-49fc-8258-eb49ab9db090",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebe689ef-49b5-4686-b698-460d6a1ab65e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8617fb2-386c-4d5d-81f9-1a9e66648978",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebe689ef-49b5-4686-b698-460d6a1ab65e",
                    "LayerId": "8e569032-93c0-4496-8f46-c7d0620fb475"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8e569032-93c0-4496-8f46-c7d0620fb475",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58fdf3ae-c7e3-41fb-bc9e-5845e2c1d87c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}