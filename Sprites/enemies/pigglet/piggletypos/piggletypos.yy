{
    "id": "b4232f30-272e-4e5d-afd7-f860f6012156",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "piggletypos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 8,
    "bbox_right": 23,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2821932a-4780-4947-80a4-075ff1f10203",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4232f30-272e-4e5d-afd7-f860f6012156",
            "compositeImage": {
                "id": "a52caa17-4602-4768-afeb-7dbcb3e353b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2821932a-4780-4947-80a4-075ff1f10203",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1924e44f-5643-4517-b516-3984b6fd60a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2821932a-4780-4947-80a4-075ff1f10203",
                    "LayerId": "a32be476-1507-44be-97c0-f71074f574d1"
                }
            ]
        },
        {
            "id": "999ae643-ae8e-45df-a30f-1401b8491f37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4232f30-272e-4e5d-afd7-f860f6012156",
            "compositeImage": {
                "id": "46f90ccb-efc6-48ae-97f6-64ff891e4c0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "999ae643-ae8e-45df-a30f-1401b8491f37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20979376-3134-49cf-a4a8-f76366fb5312",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "999ae643-ae8e-45df-a30f-1401b8491f37",
                    "LayerId": "a32be476-1507-44be-97c0-f71074f574d1"
                }
            ]
        },
        {
            "id": "0a27e7f7-d74b-4097-a86a-a97a846facb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4232f30-272e-4e5d-afd7-f860f6012156",
            "compositeImage": {
                "id": "6e412780-25e4-4440-a848-ef28b8e20d44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a27e7f7-d74b-4097-a86a-a97a846facb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6dc17d7-8e6a-466c-96f3-4d51896880cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a27e7f7-d74b-4097-a86a-a97a846facb3",
                    "LayerId": "a32be476-1507-44be-97c0-f71074f574d1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a32be476-1507-44be-97c0-f71074f574d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b4232f30-272e-4e5d-afd7-f860f6012156",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}