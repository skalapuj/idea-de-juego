{
    "id": "639a8943-216a-422c-9c57-9943637d1066",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "piggletxpos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 6,
    "bbox_right": 27,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "31885cc3-de11-4736-b1a2-3efc8f16ab96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "639a8943-216a-422c-9c57-9943637d1066",
            "compositeImage": {
                "id": "1bdce9fc-8073-46a4-ac53-6785d76d19fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31885cc3-de11-4736-b1a2-3efc8f16ab96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61723ded-def0-48f9-98ad-31c7861fba80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31885cc3-de11-4736-b1a2-3efc8f16ab96",
                    "LayerId": "88ee3062-2bbc-447c-bd32-4e180c0b0d8b"
                }
            ]
        },
        {
            "id": "b7947052-46f2-4ca2-8944-53ec5231a476",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "639a8943-216a-422c-9c57-9943637d1066",
            "compositeImage": {
                "id": "f939f3b6-8d79-4ce4-bc14-e0d64f476917",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7947052-46f2-4ca2-8944-53ec5231a476",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1129cf02-da89-4c85-8e77-e9abb75b7f2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7947052-46f2-4ca2-8944-53ec5231a476",
                    "LayerId": "88ee3062-2bbc-447c-bd32-4e180c0b0d8b"
                }
            ]
        },
        {
            "id": "d951d5a1-026e-4f25-83c8-5a246bf672bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "639a8943-216a-422c-9c57-9943637d1066",
            "compositeImage": {
                "id": "6366a351-8fbc-41a6-9d93-efc19bfda099",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d951d5a1-026e-4f25-83c8-5a246bf672bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9b27f1a-2519-4cbb-bc1f-13527ce65533",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d951d5a1-026e-4f25-83c8-5a246bf672bd",
                    "LayerId": "88ee3062-2bbc-447c-bd32-4e180c0b0d8b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "88ee3062-2bbc-447c-bd32-4e180c0b0d8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "639a8943-216a-422c-9c57-9943637d1066",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}