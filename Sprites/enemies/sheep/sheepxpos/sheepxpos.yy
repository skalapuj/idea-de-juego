{
    "id": "1a13b531-3dc2-458d-aea1-e0b0513d5c58",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sheepxpos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 25,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "74e2aeae-78e6-44bf-a8ff-2e7e01ade093",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a13b531-3dc2-458d-aea1-e0b0513d5c58",
            "compositeImage": {
                "id": "4005e75d-ea38-4307-b12d-e26f9ca720f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74e2aeae-78e6-44bf-a8ff-2e7e01ade093",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c99c8d92-bc08-4ddd-ba48-a44ef223edd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74e2aeae-78e6-44bf-a8ff-2e7e01ade093",
                    "LayerId": "f836c75a-9c23-48f6-8652-9d465795c6fb"
                }
            ]
        },
        {
            "id": "4cecc5f9-583d-4db6-986b-b9065792b95f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a13b531-3dc2-458d-aea1-e0b0513d5c58",
            "compositeImage": {
                "id": "34f362d9-4890-41b8-ad4f-8a598aa85888",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cecc5f9-583d-4db6-986b-b9065792b95f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d8e7bfc-bc05-4a6c-a73e-4d3986dcbb66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cecc5f9-583d-4db6-986b-b9065792b95f",
                    "LayerId": "f836c75a-9c23-48f6-8652-9d465795c6fb"
                }
            ]
        },
        {
            "id": "4c45d075-146b-4153-91d4-28f06b871b23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a13b531-3dc2-458d-aea1-e0b0513d5c58",
            "compositeImage": {
                "id": "99afbdf3-086d-4ffa-9304-76afee1c4785",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c45d075-146b-4153-91d4-28f06b871b23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c3779d9-15a9-43e5-85e3-130257f653fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c45d075-146b-4153-91d4-28f06b871b23",
                    "LayerId": "f836c75a-9c23-48f6-8652-9d465795c6fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "f836c75a-9c23-48f6-8652-9d465795c6fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a13b531-3dc2-458d-aea1-e0b0513d5c58",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}