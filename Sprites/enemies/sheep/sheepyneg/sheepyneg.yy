{
    "id": "3c0cc437-416a-47dc-9180-76ca5a4a11a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sheepyneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 7,
    "bbox_right": 24,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ffbea3e2-d9ee-41fc-b1c7-679aa70b480d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c0cc437-416a-47dc-9180-76ca5a4a11a9",
            "compositeImage": {
                "id": "4a0d790b-4f5f-4e90-8634-8fd18617f085",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffbea3e2-d9ee-41fc-b1c7-679aa70b480d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c20f9dbc-1708-4d17-a956-da3bb8eb1b8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffbea3e2-d9ee-41fc-b1c7-679aa70b480d",
                    "LayerId": "673f64a4-6a0c-498d-be40-3f286aaedf95"
                }
            ]
        },
        {
            "id": "2e4d1aac-ab8a-4528-86ef-474be26dc1e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c0cc437-416a-47dc-9180-76ca5a4a11a9",
            "compositeImage": {
                "id": "f3ff454b-d010-4b0c-ba9e-6156473b6bc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e4d1aac-ab8a-4528-86ef-474be26dc1e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed68c59d-dbd6-4d11-a27e-ea936b458e77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e4d1aac-ab8a-4528-86ef-474be26dc1e7",
                    "LayerId": "673f64a4-6a0c-498d-be40-3f286aaedf95"
                }
            ]
        },
        {
            "id": "bed66df3-ffd2-4dd1-a791-6f24ce5e6098",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c0cc437-416a-47dc-9180-76ca5a4a11a9",
            "compositeImage": {
                "id": "dd438c6a-a656-424a-a601-4d91d47c7c00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bed66df3-ffd2-4dd1-a791-6f24ce5e6098",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6dcb62c-86f2-4701-b6f4-9e590545fc64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bed66df3-ffd2-4dd1-a791-6f24ce5e6098",
                    "LayerId": "673f64a4-6a0c-498d-be40-3f286aaedf95"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "673f64a4-6a0c-498d-be40-3f286aaedf95",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c0cc437-416a-47dc-9180-76ca5a4a11a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}