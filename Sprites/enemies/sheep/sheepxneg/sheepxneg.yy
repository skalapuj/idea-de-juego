{
    "id": "b74369ae-4a04-429e-bc56-bb6bd943f687",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sheepxneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "df96a3a5-94a2-4500-a39c-0f7ed5de30a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b74369ae-4a04-429e-bc56-bb6bd943f687",
            "compositeImage": {
                "id": "7d28767d-b356-4127-9f1e-229cb7fa99f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df96a3a5-94a2-4500-a39c-0f7ed5de30a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9216863-e0c1-4d87-b19d-d6897c2268df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df96a3a5-94a2-4500-a39c-0f7ed5de30a8",
                    "LayerId": "9d4f7a16-a826-4d2a-8261-61c0dd365cf3"
                }
            ]
        },
        {
            "id": "9b0748ab-9a7b-476e-9629-e46712e5d386",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b74369ae-4a04-429e-bc56-bb6bd943f687",
            "compositeImage": {
                "id": "f0b54032-b337-4417-8187-9b12a82aa329",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b0748ab-9a7b-476e-9629-e46712e5d386",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9601ffb1-03fc-4298-af83-d0f998b16544",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b0748ab-9a7b-476e-9629-e46712e5d386",
                    "LayerId": "9d4f7a16-a826-4d2a-8261-61c0dd365cf3"
                }
            ]
        },
        {
            "id": "d2210825-15c1-4d5d-b0e3-207d3ef79575",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b74369ae-4a04-429e-bc56-bb6bd943f687",
            "compositeImage": {
                "id": "65345f95-e16f-48dd-a755-64c1395ea9ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2210825-15c1-4d5d-b0e3-207d3ef79575",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b0766a6-2f64-4285-afc0-f0881ad24746",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2210825-15c1-4d5d-b0e3-207d3ef79575",
                    "LayerId": "9d4f7a16-a826-4d2a-8261-61c0dd365cf3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "9d4f7a16-a826-4d2a-8261-61c0dd365cf3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b74369ae-4a04-429e-bc56-bb6bd943f687",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}