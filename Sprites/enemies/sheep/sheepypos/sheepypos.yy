{
    "id": "cd469f83-9633-43a6-aa7e-1c767037f1c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sheepypos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96b360fd-fcf9-497b-875e-a4d764c8edea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd469f83-9633-43a6-aa7e-1c767037f1c8",
            "compositeImage": {
                "id": "de3129ca-6733-46c6-844c-b7ddcdc234b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96b360fd-fcf9-497b-875e-a4d764c8edea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "770431e1-0ff4-41bf-b0e6-14b667b052f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96b360fd-fcf9-497b-875e-a4d764c8edea",
                    "LayerId": "8792cc6e-49e8-4eec-8aae-7697b89abf10"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8792cc6e-49e8-4eec-8aae-7697b89abf10",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd469f83-9633-43a6-aa7e-1c767037f1c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}