{
    "id": "b116b555-5fea-455b-871a-891a38581ab7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "butterflyxneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 3,
    "bbox_right": 15,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6d603532-d1da-4b10-8a80-dc37bbdacb48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b116b555-5fea-455b-871a-891a38581ab7",
            "compositeImage": {
                "id": "47195b7b-548f-4803-958f-b6df1c347a2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d603532-d1da-4b10-8a80-dc37bbdacb48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c70e4f9-56ab-4c23-9aca-0c2477d17d78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d603532-d1da-4b10-8a80-dc37bbdacb48",
                    "LayerId": "0be7fdea-56cc-47f1-a867-8630390e518b"
                }
            ]
        },
        {
            "id": "5a3e126d-d1c5-4ebf-8968-5b72ea3f5080",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b116b555-5fea-455b-871a-891a38581ab7",
            "compositeImage": {
                "id": "aa1bdb2a-485d-4aed-b71e-e83f97530c8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a3e126d-d1c5-4ebf-8968-5b72ea3f5080",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6b379ca-1f2f-4e54-bb41-d1414099737c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a3e126d-d1c5-4ebf-8968-5b72ea3f5080",
                    "LayerId": "0be7fdea-56cc-47f1-a867-8630390e518b"
                }
            ]
        },
        {
            "id": "397a85f9-0b9c-4ae0-88d4-7cb700858958",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b116b555-5fea-455b-871a-891a38581ab7",
            "compositeImage": {
                "id": "70d3afea-61c5-4681-901e-301bd78b87d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "397a85f9-0b9c-4ae0-88d4-7cb700858958",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf8835b0-7b83-4124-a2f2-883f0982354e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "397a85f9-0b9c-4ae0-88d4-7cb700858958",
                    "LayerId": "0be7fdea-56cc-47f1-a867-8630390e518b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 26,
    "layers": [
        {
            "id": "0be7fdea-56cc-47f1-a867-8630390e518b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b116b555-5fea-455b-871a-891a38581ab7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 0,
    "yorig": 0
}