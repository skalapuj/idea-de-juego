{
    "id": "f03e7552-c82a-48c4-ba2c-e0a8f5971b26",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "butterflyypos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 2,
    "bbox_right": 18,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "38f85928-d490-4e32-877f-a417683faa8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f03e7552-c82a-48c4-ba2c-e0a8f5971b26",
            "compositeImage": {
                "id": "56bb5efc-d701-4d98-9362-18b77ee1effd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38f85928-d490-4e32-877f-a417683faa8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61c3f8a1-3445-4c42-8fba-7ae7333e2333",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38f85928-d490-4e32-877f-a417683faa8d",
                    "LayerId": "1c179435-5c9b-423a-9ab2-02045754a74e"
                }
            ]
        },
        {
            "id": "9a38dbfd-49ad-4c33-a9f8-91e1e97747bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f03e7552-c82a-48c4-ba2c-e0a8f5971b26",
            "compositeImage": {
                "id": "ca7b0333-4ce2-45e1-a12b-2974f4b3a07d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a38dbfd-49ad-4c33-a9f8-91e1e97747bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "944be3c2-aece-4567-8495-b5094e0e6335",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a38dbfd-49ad-4c33-a9f8-91e1e97747bd",
                    "LayerId": "1c179435-5c9b-423a-9ab2-02045754a74e"
                }
            ]
        },
        {
            "id": "e621170d-edac-42d0-a565-694de2f94b0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f03e7552-c82a-48c4-ba2c-e0a8f5971b26",
            "compositeImage": {
                "id": "6d094637-dd96-4b0a-aa68-e73b92661232",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e621170d-edac-42d0-a565-694de2f94b0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f70b665f-b969-4dd7-a3f2-354d6db36adb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e621170d-edac-42d0-a565-694de2f94b0a",
                    "LayerId": "1c179435-5c9b-423a-9ab2-02045754a74e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 26,
    "layers": [
        {
            "id": "1c179435-5c9b-423a-9ab2-02045754a74e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f03e7552-c82a-48c4-ba2c-e0a8f5971b26",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 0,
    "yorig": 0
}