{
    "id": "f475946f-7ad0-41dc-b0dd-98f089c607d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "butterflyyneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 4,
    "bbox_right": 20,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e5906bb3-5989-46c2-8b7c-1314406bc3fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f475946f-7ad0-41dc-b0dd-98f089c607d0",
            "compositeImage": {
                "id": "36ea4fd4-712a-4612-add6-fe8e56446f11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5906bb3-5989-46c2-8b7c-1314406bc3fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "006a0c18-fbb1-43be-bbdf-8b250c186d60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5906bb3-5989-46c2-8b7c-1314406bc3fd",
                    "LayerId": "ac02427b-2b13-42eb-b714-4879d863d959"
                }
            ]
        },
        {
            "id": "d82978c3-4b95-425e-a46f-4371f0696023",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f475946f-7ad0-41dc-b0dd-98f089c607d0",
            "compositeImage": {
                "id": "f503bf8f-8445-4f28-a54a-e5f35126f8c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d82978c3-4b95-425e-a46f-4371f0696023",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9b6666f-cb56-49e2-8d89-ab195d9cda7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d82978c3-4b95-425e-a46f-4371f0696023",
                    "LayerId": "ac02427b-2b13-42eb-b714-4879d863d959"
                }
            ]
        },
        {
            "id": "7db11449-f252-4b5a-bf23-07580a5085ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f475946f-7ad0-41dc-b0dd-98f089c607d0",
            "compositeImage": {
                "id": "465b6789-5a45-490b-8926-18525f8be9c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7db11449-f252-4b5a-bf23-07580a5085ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e56e715-23ea-40b9-aa84-1394bfedcf8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7db11449-f252-4b5a-bf23-07580a5085ca",
                    "LayerId": "ac02427b-2b13-42eb-b714-4879d863d959"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 26,
    "layers": [
        {
            "id": "ac02427b-2b13-42eb-b714-4879d863d959",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f475946f-7ad0-41dc-b0dd-98f089c607d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 0,
    "yorig": 0
}