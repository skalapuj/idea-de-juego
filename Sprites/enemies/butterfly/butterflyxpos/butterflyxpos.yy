{
    "id": "865a3560-0c56-4766-9455-5436149b5b62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "butterflyxpos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05964d61-ad65-4070-b61b-f8410b1c64a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "865a3560-0c56-4766-9455-5436149b5b62",
            "compositeImage": {
                "id": "779bb03d-bfa4-4779-a26d-fc57e881c3e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05964d61-ad65-4070-b61b-f8410b1c64a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "998c5f98-4ab9-405a-b285-d2328b407382",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05964d61-ad65-4070-b61b-f8410b1c64a4",
                    "LayerId": "076b6b6a-2c7a-42fb-a46a-11dcd3cee322"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "076b6b6a-2c7a-42fb-a46a-11dcd3cee322",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "865a3560-0c56-4766-9455-5436149b5b62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}