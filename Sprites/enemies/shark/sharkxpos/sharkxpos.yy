{
    "id": "5ccd570c-0edc-43bc-85e8-d0c74ae9e97d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sharkxpos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f35b77e-dd52-4bef-bb5b-109821a90a29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ccd570c-0edc-43bc-85e8-d0c74ae9e97d",
            "compositeImage": {
                "id": "05d3042f-a885-4499-8d23-4e157d89b4cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f35b77e-dd52-4bef-bb5b-109821a90a29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2008d45-efab-4892-aed8-cb1bfd70a54e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f35b77e-dd52-4bef-bb5b-109821a90a29",
                    "LayerId": "33b8a352-4a4f-4c02-9aec-ee54385653ab"
                }
            ]
        },
        {
            "id": "3aa76ef6-8864-4006-a7c2-c036d2409ced",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ccd570c-0edc-43bc-85e8-d0c74ae9e97d",
            "compositeImage": {
                "id": "bcd990a7-eeba-4d71-944d-607eaf3233dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3aa76ef6-8864-4006-a7c2-c036d2409ced",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "316e1f84-5cdf-4e19-8dbe-2b8fac9c4c5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3aa76ef6-8864-4006-a7c2-c036d2409ced",
                    "LayerId": "33b8a352-4a4f-4c02-9aec-ee54385653ab"
                }
            ]
        },
        {
            "id": "8befa0d7-5941-4a3e-8b84-6c0063edcb0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ccd570c-0edc-43bc-85e8-d0c74ae9e97d",
            "compositeImage": {
                "id": "e8ca3f2f-5766-473f-8739-8897d1ef9a2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8befa0d7-5941-4a3e-8b84-6c0063edcb0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "131d1230-34a8-45a2-aede-31c30aca831c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8befa0d7-5941-4a3e-8b84-6c0063edcb0e",
                    "LayerId": "33b8a352-4a4f-4c02-9aec-ee54385653ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "33b8a352-4a4f-4c02-9aec-ee54385653ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ccd570c-0edc-43bc-85e8-d0c74ae9e97d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}