{
    "id": "c823e7b3-e37c-46a9-924f-16bdec3e4a00",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sharkxneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 1,
    "bbox_right": 31,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "77d164d1-65de-47e0-a6df-5a835b3d9ca3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c823e7b3-e37c-46a9-924f-16bdec3e4a00",
            "compositeImage": {
                "id": "90adcd28-7683-4ebb-8281-21f98d6b26dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77d164d1-65de-47e0-a6df-5a835b3d9ca3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4a51ef5-36ce-4492-8bc4-c561733cc798",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77d164d1-65de-47e0-a6df-5a835b3d9ca3",
                    "LayerId": "1398e73f-0ac6-409d-b842-6bba7cce58a2"
                }
            ]
        },
        {
            "id": "7758e034-dbc3-4e16-806e-1a5334bfc8ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c823e7b3-e37c-46a9-924f-16bdec3e4a00",
            "compositeImage": {
                "id": "1cf624c8-eb57-41c5-b242-8a5b21617e76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7758e034-dbc3-4e16-806e-1a5334bfc8ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6481b21a-0959-4b2c-9be5-f5d7a5c9fe4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7758e034-dbc3-4e16-806e-1a5334bfc8ae",
                    "LayerId": "1398e73f-0ac6-409d-b842-6bba7cce58a2"
                }
            ]
        },
        {
            "id": "2227a64c-ce45-46d5-896b-e3464cd2270f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c823e7b3-e37c-46a9-924f-16bdec3e4a00",
            "compositeImage": {
                "id": "305af654-4a36-471a-b940-f3b04f567c4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2227a64c-ce45-46d5-896b-e3464cd2270f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e497f132-54e6-4fce-adf9-420d718f3733",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2227a64c-ce45-46d5-896b-e3464cd2270f",
                    "LayerId": "1398e73f-0ac6-409d-b842-6bba7cce58a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1398e73f-0ac6-409d-b842-6bba7cce58a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c823e7b3-e37c-46a9-924f-16bdec3e4a00",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}