{
    "id": "906c81bc-573b-4344-a488-a9c11f8e3312",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sharkyneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d2b5c5d-cc2c-4691-8468-daf25b623503",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "906c81bc-573b-4344-a488-a9c11f8e3312",
            "compositeImage": {
                "id": "3de9cc63-5250-408f-8b31-33c2716a4565",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d2b5c5d-cc2c-4691-8468-daf25b623503",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf7ef0f2-4245-45ec-a2a1-5d028e84a892",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d2b5c5d-cc2c-4691-8468-daf25b623503",
                    "LayerId": "5ffb9330-0b21-4cba-96f4-5f40b1cfa03a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5ffb9330-0b21-4cba-96f4-5f40b1cfa03a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "906c81bc-573b-4344-a488-a9c11f8e3312",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}