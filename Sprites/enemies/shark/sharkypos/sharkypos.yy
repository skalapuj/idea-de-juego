{
    "id": "fc8285af-278f-46ed-b692-0239facab8dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sharkypos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 2,
    "bbox_right": 27,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9c5bafef-7355-4a7e-8fa6-271924e1eb7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc8285af-278f-46ed-b692-0239facab8dd",
            "compositeImage": {
                "id": "484da66f-40b1-473d-937b-17e8d4e21c73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c5bafef-7355-4a7e-8fa6-271924e1eb7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e27e15fb-417e-4de2-a20c-e24951a85842",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c5bafef-7355-4a7e-8fa6-271924e1eb7b",
                    "LayerId": "52b9bc05-61fe-482c-8494-3ef8548c17cb"
                }
            ]
        },
        {
            "id": "c1a150e4-c3c8-4d2f-bfa8-ad97f72c0622",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc8285af-278f-46ed-b692-0239facab8dd",
            "compositeImage": {
                "id": "bb831e03-dfcd-4e56-a408-83b750a5d028",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1a150e4-c3c8-4d2f-bfa8-ad97f72c0622",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0d888db-cde1-45b4-b84d-0849cfa92dc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1a150e4-c3c8-4d2f-bfa8-ad97f72c0622",
                    "LayerId": "52b9bc05-61fe-482c-8494-3ef8548c17cb"
                }
            ]
        },
        {
            "id": "091ef344-52bd-48eb-8dc9-81aa022ec072",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc8285af-278f-46ed-b692-0239facab8dd",
            "compositeImage": {
                "id": "90f7e64c-ff8c-4a8d-8b84-66dfa0294a8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "091ef344-52bd-48eb-8dc9-81aa022ec072",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ebe5388-4f87-4967-b4f8-8d9be7f001ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "091ef344-52bd-48eb-8dc9-81aa022ec072",
                    "LayerId": "52b9bc05-61fe-482c-8494-3ef8548c17cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "52b9bc05-61fe-482c-8494-3ef8548c17cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc8285af-278f-46ed-b692-0239facab8dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}