{
    "id": "5e2375fb-5cc7-466e-80e6-8f7588ce4dcf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "piggieypos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 8,
    "bbox_right": 23,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6652c40d-352e-4d06-a46f-f6fba7891118",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e2375fb-5cc7-466e-80e6-8f7588ce4dcf",
            "compositeImage": {
                "id": "375bf0f7-6277-4cba-bdbd-6278ed81a368",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6652c40d-352e-4d06-a46f-f6fba7891118",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4946bb9d-3014-4287-a27a-4553f3c7418a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6652c40d-352e-4d06-a46f-f6fba7891118",
                    "LayerId": "52bda612-e805-4bd7-854b-44286558add8"
                }
            ]
        },
        {
            "id": "4be91083-a79f-4a35-8dc5-06de961ad4cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e2375fb-5cc7-466e-80e6-8f7588ce4dcf",
            "compositeImage": {
                "id": "10956381-8a54-4aca-b897-469907f65a37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4be91083-a79f-4a35-8dc5-06de961ad4cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f96480ea-921f-4a98-909f-179f986a8973",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4be91083-a79f-4a35-8dc5-06de961ad4cd",
                    "LayerId": "52bda612-e805-4bd7-854b-44286558add8"
                }
            ]
        },
        {
            "id": "99001c26-61b1-4207-a7f4-f4573f032a8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e2375fb-5cc7-466e-80e6-8f7588ce4dcf",
            "compositeImage": {
                "id": "172ad20b-b8ff-4fdc-8cb5-d6ed1bb096f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99001c26-61b1-4207-a7f4-f4573f032a8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7f4d8d4-c628-4f70-aa2d-89fc6563ff75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99001c26-61b1-4207-a7f4-f4573f032a8b",
                    "LayerId": "52bda612-e805-4bd7-854b-44286558add8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "52bda612-e805-4bd7-854b-44286558add8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e2375fb-5cc7-466e-80e6-8f7588ce4dcf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}