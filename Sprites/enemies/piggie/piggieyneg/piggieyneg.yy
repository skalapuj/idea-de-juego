{
    "id": "1ab5d9e2-4291-47bb-8c06-c2158831eb20",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "piggieyneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 8,
    "bbox_right": 23,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f943c8d-8bc3-42fd-b245-15e3a43f8cda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ab5d9e2-4291-47bb-8c06-c2158831eb20",
            "compositeImage": {
                "id": "55bd8271-ab57-47c3-b1c4-d3748c42da5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f943c8d-8bc3-42fd-b245-15e3a43f8cda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3aae4818-fb82-4dad-85e1-57c01fdd102d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f943c8d-8bc3-42fd-b245-15e3a43f8cda",
                    "LayerId": "5c361dc5-b9a6-442a-84b2-992f15352954"
                }
            ]
        },
        {
            "id": "9ae90ad0-af88-43d7-9141-19a130a4d479",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ab5d9e2-4291-47bb-8c06-c2158831eb20",
            "compositeImage": {
                "id": "bd6be9a5-b175-47cb-8fcf-3ffbc0d5242a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ae90ad0-af88-43d7-9141-19a130a4d479",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e134304-4e75-401d-8b12-d02a5d3b120a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ae90ad0-af88-43d7-9141-19a130a4d479",
                    "LayerId": "5c361dc5-b9a6-442a-84b2-992f15352954"
                }
            ]
        },
        {
            "id": "26e2aa42-997a-4b14-b263-86ea50c51d63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ab5d9e2-4291-47bb-8c06-c2158831eb20",
            "compositeImage": {
                "id": "975e398f-4ac8-413c-8bb7-75ad995b3954",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26e2aa42-997a-4b14-b263-86ea50c51d63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc788eda-a17d-498f-b1d8-54c80aff23ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26e2aa42-997a-4b14-b263-86ea50c51d63",
                    "LayerId": "5c361dc5-b9a6-442a-84b2-992f15352954"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5c361dc5-b9a6-442a-84b2-992f15352954",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ab5d9e2-4291-47bb-8c06-c2158831eb20",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}