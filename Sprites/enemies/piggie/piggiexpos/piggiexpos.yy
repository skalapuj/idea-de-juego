{
    "id": "5cc3f564-44f7-4bdb-8cbd-d1e48eb9b3b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "piggiexpos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 6,
    "bbox_right": 27,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1cda453a-4eca-4bff-8bb2-d7e03ebdadf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cc3f564-44f7-4bdb-8cbd-d1e48eb9b3b3",
            "compositeImage": {
                "id": "d47dd931-6897-4a0f-aac4-fab1c3a18dcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cda453a-4eca-4bff-8bb2-d7e03ebdadf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82fbd300-b914-4b76-84b2-15e1d6dbff94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cda453a-4eca-4bff-8bb2-d7e03ebdadf0",
                    "LayerId": "062bfb84-fca1-4545-8c02-a41471586914"
                }
            ]
        },
        {
            "id": "62b5d4a9-91de-450e-b12c-ba2bab5eccc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cc3f564-44f7-4bdb-8cbd-d1e48eb9b3b3",
            "compositeImage": {
                "id": "3412c41a-fc0d-409e-8b84-b8b28225112e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62b5d4a9-91de-450e-b12c-ba2bab5eccc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c87d46a-4507-4533-b734-03af16fa30f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62b5d4a9-91de-450e-b12c-ba2bab5eccc1",
                    "LayerId": "062bfb84-fca1-4545-8c02-a41471586914"
                }
            ]
        },
        {
            "id": "aa035342-b713-437e-9ea8-fcdd396f7f01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cc3f564-44f7-4bdb-8cbd-d1e48eb9b3b3",
            "compositeImage": {
                "id": "14a3f8d7-d916-46ff-a128-3008fc3079b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa035342-b713-437e-9ea8-fcdd396f7f01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb7ee25f-81fd-4dce-b95a-29defc797331",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa035342-b713-437e-9ea8-fcdd396f7f01",
                    "LayerId": "062bfb84-fca1-4545-8c02-a41471586914"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "062bfb84-fca1-4545-8c02-a41471586914",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5cc3f564-44f7-4bdb-8cbd-d1e48eb9b3b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}