{
    "id": "d2dd99e1-7e92-4c3e-bd79-bd3497fd3120",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "piggiexneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 4,
    "bbox_right": 25,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7016af26-4dea-4903-bef3-913c4655a116",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2dd99e1-7e92-4c3e-bd79-bd3497fd3120",
            "compositeImage": {
                "id": "c1f92899-92fc-4256-92be-68a8877f4a09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7016af26-4dea-4903-bef3-913c4655a116",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a5e9a73-6170-4b8d-8939-e72d7dc58bbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7016af26-4dea-4903-bef3-913c4655a116",
                    "LayerId": "0e12f8e7-ffe2-437a-9f9c-1f96d38e0b64"
                }
            ]
        },
        {
            "id": "426577ad-27e1-4bf3-b1a9-8cb6a148f573",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2dd99e1-7e92-4c3e-bd79-bd3497fd3120",
            "compositeImage": {
                "id": "2c636071-a4ec-4f3f-8a5c-7c2afcacca3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "426577ad-27e1-4bf3-b1a9-8cb6a148f573",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22c580b2-77c1-498d-90ff-79051ee635bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "426577ad-27e1-4bf3-b1a9-8cb6a148f573",
                    "LayerId": "0e12f8e7-ffe2-437a-9f9c-1f96d38e0b64"
                }
            ]
        },
        {
            "id": "9f5f4640-26ca-4215-9b57-d883cf4d3e35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2dd99e1-7e92-4c3e-bd79-bd3497fd3120",
            "compositeImage": {
                "id": "c3bfd449-3954-4a73-95ca-1b3d8e47ec2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f5f4640-26ca-4215-9b57-d883cf4d3e35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de2716af-9e53-46be-b32e-34fc3dbaf6fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f5f4640-26ca-4215-9b57-d883cf4d3e35",
                    "LayerId": "0e12f8e7-ffe2-437a-9f9c-1f96d38e0b64"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0e12f8e7-ffe2-437a-9f9c-1f96d38e0b64",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d2dd99e1-7e92-4c3e-bd79-bd3497fd3120",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}