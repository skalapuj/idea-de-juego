{
    "id": "9bc054d3-6dcb-4dc3-92e4-1a778b396230",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "beeypos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 3,
    "bbox_right": 20,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d91ba67-25cb-4737-ae58-93f97b4a39a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9bc054d3-6dcb-4dc3-92e4-1a778b396230",
            "compositeImage": {
                "id": "43b3d9a3-38ce-43fe-8b6d-579da4c27652",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d91ba67-25cb-4737-ae58-93f97b4a39a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab7e9d33-64c9-4416-b62c-b674b7e8bde0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d91ba67-25cb-4737-ae58-93f97b4a39a8",
                    "LayerId": "5c455421-0f6d-4336-9def-f45ea735f0e8"
                }
            ]
        },
        {
            "id": "87d6ab17-f109-458f-9769-80bda9d5c571",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9bc054d3-6dcb-4dc3-92e4-1a778b396230",
            "compositeImage": {
                "id": "369a1de4-4762-46d4-815c-7e1cf54d1610",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87d6ab17-f109-458f-9769-80bda9d5c571",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44802f89-dcf1-4034-8326-1f4d22182fe3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87d6ab17-f109-458f-9769-80bda9d5c571",
                    "LayerId": "5c455421-0f6d-4336-9def-f45ea735f0e8"
                }
            ]
        },
        {
            "id": "572fd418-4558-4da0-86f9-7abc2ef6a8a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9bc054d3-6dcb-4dc3-92e4-1a778b396230",
            "compositeImage": {
                "id": "1abb1ead-364d-489f-b174-aca8a36b4e74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "572fd418-4558-4da0-86f9-7abc2ef6a8a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbc63d60-a8c1-4c80-aeee-824654e6925c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "572fd418-4558-4da0-86f9-7abc2ef6a8a1",
                    "LayerId": "5c455421-0f6d-4336-9def-f45ea735f0e8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 26,
    "layers": [
        {
            "id": "5c455421-0f6d-4336-9def-f45ea735f0e8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9bc054d3-6dcb-4dc3-92e4-1a778b396230",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 0,
    "yorig": 0
}