{
    "id": "67cf2d29-dab3-481f-a7a4-fa3231c8bb06",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "beexpos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 7,
    "bbox_right": 20,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a170c0d7-acf0-4c27-9738-7cf2616b485d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67cf2d29-dab3-481f-a7a4-fa3231c8bb06",
            "compositeImage": {
                "id": "51206c31-3d78-480a-8736-a8ec378525c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a170c0d7-acf0-4c27-9738-7cf2616b485d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77ae5edd-3022-4085-883d-9ed39be18fa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a170c0d7-acf0-4c27-9738-7cf2616b485d",
                    "LayerId": "1acecfe0-d3e9-40ed-940a-2bccf405577d"
                }
            ]
        },
        {
            "id": "a8ee0eec-b433-46d6-9159-5fa5f78d25c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67cf2d29-dab3-481f-a7a4-fa3231c8bb06",
            "compositeImage": {
                "id": "4ec308dc-8bd5-4ab6-ad50-517a10119ab6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8ee0eec-b433-46d6-9159-5fa5f78d25c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3452fbfb-1a14-4038-bad8-f90571fdef54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8ee0eec-b433-46d6-9159-5fa5f78d25c2",
                    "LayerId": "1acecfe0-d3e9-40ed-940a-2bccf405577d"
                }
            ]
        },
        {
            "id": "4a74a0c2-8c64-4690-8d57-99302539e33f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67cf2d29-dab3-481f-a7a4-fa3231c8bb06",
            "compositeImage": {
                "id": "6a5cb5d8-b7ca-488a-9038-a3c8d0632d49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a74a0c2-8c64-4690-8d57-99302539e33f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12cdb577-5694-40e5-b1b9-0a30254ef2d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a74a0c2-8c64-4690-8d57-99302539e33f",
                    "LayerId": "1acecfe0-d3e9-40ed-940a-2bccf405577d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 26,
    "layers": [
        {
            "id": "1acecfe0-d3e9-40ed-940a-2bccf405577d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67cf2d29-dab3-481f-a7a4-fa3231c8bb06",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 0,
    "yorig": 0
}