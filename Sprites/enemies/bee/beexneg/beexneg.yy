{
    "id": "9cb6f7b5-9b24-4811-97b6-b2e6ea53ce88",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "beexneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 4,
    "bbox_right": 17,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f5a64381-f6ba-4ea1-91e9-8879271c9168",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cb6f7b5-9b24-4811-97b6-b2e6ea53ce88",
            "compositeImage": {
                "id": "a7d4a180-49d9-4c57-b354-64a10ed62b09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5a64381-f6ba-4ea1-91e9-8879271c9168",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e248994-8588-4821-a977-220d713cefb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5a64381-f6ba-4ea1-91e9-8879271c9168",
                    "LayerId": "2184e2fe-a4c6-4d87-9908-10fc3377a4b3"
                }
            ]
        },
        {
            "id": "ed45f9cc-ab86-4e5e-9ab7-e495cffa603d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cb6f7b5-9b24-4811-97b6-b2e6ea53ce88",
            "compositeImage": {
                "id": "99d323ac-6d08-4b34-87db-7f5f52186918",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed45f9cc-ab86-4e5e-9ab7-e495cffa603d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd31c436-49c3-42b4-9851-a91a73b6948a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed45f9cc-ab86-4e5e-9ab7-e495cffa603d",
                    "LayerId": "2184e2fe-a4c6-4d87-9908-10fc3377a4b3"
                }
            ]
        },
        {
            "id": "b1019272-1f7f-418b-9a2a-90e8fada355c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cb6f7b5-9b24-4811-97b6-b2e6ea53ce88",
            "compositeImage": {
                "id": "80357b0d-8a8b-4c03-b763-957393ad18df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1019272-1f7f-418b-9a2a-90e8fada355c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "683cbfbe-a2a5-4565-a306-b7d1bfd8314f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1019272-1f7f-418b-9a2a-90e8fada355c",
                    "LayerId": "2184e2fe-a4c6-4d87-9908-10fc3377a4b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 26,
    "layers": [
        {
            "id": "2184e2fe-a4c6-4d87-9908-10fc3377a4b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9cb6f7b5-9b24-4811-97b6-b2e6ea53ce88",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 0,
    "yorig": 0
}