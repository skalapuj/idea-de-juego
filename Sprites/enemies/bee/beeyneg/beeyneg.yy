{
    "id": "4a94e7fa-426b-40bd-8b65-663494601c6a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "beeyneg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 3,
    "bbox_right": 20,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19ad8a0c-bfff-4d50-b7de-c892e3b1de75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a94e7fa-426b-40bd-8b65-663494601c6a",
            "compositeImage": {
                "id": "f5228163-c8f7-423c-afce-5ccd6ae92849",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19ad8a0c-bfff-4d50-b7de-c892e3b1de75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fbfe4ce-475a-477f-b574-2cbf7c80aa9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19ad8a0c-bfff-4d50-b7de-c892e3b1de75",
                    "LayerId": "5268a97d-b0a8-4add-840a-797c596403c5"
                }
            ]
        },
        {
            "id": "030b6604-d552-493f-9926-136a973c9076",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a94e7fa-426b-40bd-8b65-663494601c6a",
            "compositeImage": {
                "id": "7f8c0c45-71d6-495f-b4d3-1c7e4597cf2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "030b6604-d552-493f-9926-136a973c9076",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7bdd5ca-f028-4b88-b642-96803f3408f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "030b6604-d552-493f-9926-136a973c9076",
                    "LayerId": "5268a97d-b0a8-4add-840a-797c596403c5"
                }
            ]
        },
        {
            "id": "dfeafa7c-151c-4f8e-a9c4-5c5a0c74a0a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a94e7fa-426b-40bd-8b65-663494601c6a",
            "compositeImage": {
                "id": "a1795ae7-f524-412a-a784-748c385c3184",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfeafa7c-151c-4f8e-a9c4-5c5a0c74a0a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcdf66c5-0faa-4516-b983-c7ac99ea0ed3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfeafa7c-151c-4f8e-a9c4-5c5a0c74a0a4",
                    "LayerId": "5268a97d-b0a8-4add-840a-797c596403c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 26,
    "layers": [
        {
            "id": "5268a97d-b0a8-4add-840a-797c596403c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4a94e7fa-426b-40bd-8b65-663494601c6a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 0,
    "yorig": 0
}