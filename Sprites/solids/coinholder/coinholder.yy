{
    "id": "bf5ad554-a40d-4c06-8fec-6bb6f5fd3cd8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "coinholder",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 2,
    "bbox_right": 17,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21dfe35a-1b7c-4d09-b0cd-b655a73bf40f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf5ad554-a40d-4c06-8fec-6bb6f5fd3cd8",
            "compositeImage": {
                "id": "2985a707-44d8-410a-aa5a-3433d8281703",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21dfe35a-1b7c-4d09-b0cd-b655a73bf40f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c069de4d-7ab0-4c53-ad35-facd824e3b6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21dfe35a-1b7c-4d09-b0cd-b655a73bf40f",
                    "LayerId": "b4239c40-224a-42f3-b84f-0d57d9eb257c"
                }
            ]
        },
        {
            "id": "22ee17a5-98cb-47d5-a750-18b2304bfb22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf5ad554-a40d-4c06-8fec-6bb6f5fd3cd8",
            "compositeImage": {
                "id": "09b273fc-9a31-4fcc-a381-e18eb282160a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22ee17a5-98cb-47d5-a750-18b2304bfb22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66a72f92-025f-4614-9a06-704d56b24481",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22ee17a5-98cb-47d5-a750-18b2304bfb22",
                    "LayerId": "b4239c40-224a-42f3-b84f-0d57d9eb257c"
                }
            ]
        },
        {
            "id": "8a0c3916-5f7b-4034-8efc-9fede6223d81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf5ad554-a40d-4c06-8fec-6bb6f5fd3cd8",
            "compositeImage": {
                "id": "6e181252-c41d-4ca0-b9d9-3d10e92333be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a0c3916-5f7b-4034-8efc-9fede6223d81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b22cc76b-d856-471e-a724-82e5e8de420d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a0c3916-5f7b-4034-8efc-9fede6223d81",
                    "LayerId": "b4239c40-224a-42f3-b84f-0d57d9eb257c"
                }
            ]
        },
        {
            "id": "3e599a32-3251-4bef-848b-a05b9b93b1e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf5ad554-a40d-4c06-8fec-6bb6f5fd3cd8",
            "compositeImage": {
                "id": "59021562-a53a-4fe9-8cf2-43865245eb86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e599a32-3251-4bef-848b-a05b9b93b1e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2beda68-ff4e-4a00-b5a1-0d188f22902a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e599a32-3251-4bef-848b-a05b9b93b1e0",
                    "LayerId": "b4239c40-224a-42f3-b84f-0d57d9eb257c"
                }
            ]
        },
        {
            "id": "2a285789-53d2-4f91-b75b-e9fbf29137ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf5ad554-a40d-4c06-8fec-6bb6f5fd3cd8",
            "compositeImage": {
                "id": "5b47f764-6a0f-4564-955d-bcd44cc84479",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a285789-53d2-4f91-b75b-e9fbf29137ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25845916-1cbd-4a87-b100-1ef4b8af452d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a285789-53d2-4f91-b75b-e9fbf29137ab",
                    "LayerId": "b4239c40-224a-42f3-b84f-0d57d9eb257c"
                }
            ]
        },
        {
            "id": "8d624805-a3c8-43a0-8a5b-d631eab72905",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf5ad554-a40d-4c06-8fec-6bb6f5fd3cd8",
            "compositeImage": {
                "id": "e310eb2d-738b-4219-99c5-1e95d09d0cf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d624805-a3c8-43a0-8a5b-d631eab72905",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8957b9a-8597-4f07-9792-6ac22f56cc34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d624805-a3c8-43a0-8a5b-d631eab72905",
                    "LayerId": "b4239c40-224a-42f3-b84f-0d57d9eb257c"
                }
            ]
        },
        {
            "id": "2d3b6d3d-568d-4898-ae68-8ab67cdc46dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf5ad554-a40d-4c06-8fec-6bb6f5fd3cd8",
            "compositeImage": {
                "id": "a9a49f34-1e26-425d-b2c0-492022ba4bd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d3b6d3d-568d-4898-ae68-8ab67cdc46dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb1709a1-9843-41ec-9be6-09eabc219fd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d3b6d3d-568d-4898-ae68-8ab67cdc46dc",
                    "LayerId": "b4239c40-224a-42f3-b84f-0d57d9eb257c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "b4239c40-224a-42f3-b84f-0d57d9eb257c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf5ad554-a40d-4c06-8fec-6bb6f5fd3cd8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}