{
    "id": "f70b408d-af2e-4811-b4cd-4517093846c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "coin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 5,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7da3aff6-1132-48d7-83bd-f83e6cad9316",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f70b408d-af2e-4811-b4cd-4517093846c1",
            "compositeImage": {
                "id": "3d47dca3-4b3b-4411-b37b-6a66d946374e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7da3aff6-1132-48d7-83bd-f83e6cad9316",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b71fbeb-af0a-4e12-b4ec-d6a09eec90d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7da3aff6-1132-48d7-83bd-f83e6cad9316",
                    "LayerId": "be164cc6-de61-4761-99bb-774040052bf6"
                }
            ]
        },
        {
            "id": "8c6459da-5214-4ded-8d64-ee89a3152624",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f70b408d-af2e-4811-b4cd-4517093846c1",
            "compositeImage": {
                "id": "c8082b09-98b8-482c-b9d2-129ce3406219",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c6459da-5214-4ded-8d64-ee89a3152624",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "527f2ea9-6696-4139-bb2e-22a80a294452",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c6459da-5214-4ded-8d64-ee89a3152624",
                    "LayerId": "be164cc6-de61-4761-99bb-774040052bf6"
                }
            ]
        },
        {
            "id": "a7745a9e-9822-4269-bfe1-bfd56820b5b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f70b408d-af2e-4811-b4cd-4517093846c1",
            "compositeImage": {
                "id": "78fce4be-a872-4842-93ee-3ef4853ede39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7745a9e-9822-4269-bfe1-bfd56820b5b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "243bf227-d231-4780-8b45-c0342e8bebe4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7745a9e-9822-4269-bfe1-bfd56820b5b5",
                    "LayerId": "be164cc6-de61-4761-99bb-774040052bf6"
                }
            ]
        },
        {
            "id": "f1f34f5b-cead-40f1-8b9a-6a704cf75fa0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f70b408d-af2e-4811-b4cd-4517093846c1",
            "compositeImage": {
                "id": "0b179c61-5366-44fe-824d-36c9920ab274",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1f34f5b-cead-40f1-8b9a-6a704cf75fa0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b15510a8-700f-43f9-beab-aa992184879d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1f34f5b-cead-40f1-8b9a-6a704cf75fa0",
                    "LayerId": "be164cc6-de61-4761-99bb-774040052bf6"
                }
            ]
        },
        {
            "id": "4d55df62-9323-4dc2-831f-b1847eebaec5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f70b408d-af2e-4811-b4cd-4517093846c1",
            "compositeImage": {
                "id": "19d6971e-a8e2-486b-baff-02279b0d2e69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d55df62-9323-4dc2-831f-b1847eebaec5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "996e3726-718e-4491-a9a8-5383bd5bb0c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d55df62-9323-4dc2-831f-b1847eebaec5",
                    "LayerId": "be164cc6-de61-4761-99bb-774040052bf6"
                }
            ]
        },
        {
            "id": "5a10adf8-a763-469b-9372-5d1f2011adc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f70b408d-af2e-4811-b4cd-4517093846c1",
            "compositeImage": {
                "id": "78620484-c7e1-4fda-8bd6-672636730ccc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a10adf8-a763-469b-9372-5d1f2011adc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc0aebb1-0511-4806-9569-79ac377a8953",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a10adf8-a763-469b-9372-5d1f2011adc1",
                    "LayerId": "be164cc6-de61-4761-99bb-774040052bf6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "be164cc6-de61-4761-99bb-774040052bf6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f70b408d-af2e-4811-b4cd-4517093846c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}